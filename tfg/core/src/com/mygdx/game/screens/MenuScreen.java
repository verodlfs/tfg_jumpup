package com.mygdx.game.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.kotcrab.vis.ui.VisUI;
import com.mygdx.game.MyGdxGame;
import com.mygdx.game.managers.ResourceManager;
import com.mygdx.game.util.Constants;

/**
 * Clase del menu inicial
 */
public class MenuScreen implements Screen {

    final MyGdxGame game;
    private Stage stage;
    private Table table;
    private Texture texture;
	private SpriteBatch batch;
    private Viewport port;
    /**
     * Constructor
     * @param game MyGdxGame
     */
    public MenuScreen(final MyGdxGame game) {
        this.game = game;
    }

    /**
     * Se muestra la imagen de la portada,
     * los botones con su funcionalidad
     */
    @Override
    public void show() {

        game.level = 1;

    	batch = new SpriteBatch();
    	texture = new Texture(Gdx.files.internal("textures/images/portada.png"));

        stage = new Stage();
        this.port = new FitViewport(Constants.SCREEN_WIDTH,Constants.SCREEN_HEIGHT,new OrthographicCamera());
        stage = new Stage(port);
        Gdx.input.setInputProcessor(stage);
        table = new Table();

        table.setPosition(Constants.SCREEN_WIDTH-350, 500f);

        //el orden importa si no la imagen tapa la tabla
        stage.addActor(new Image(texture));
        stage.addActor(table);


        Skin style = ResourceManager.getSkin("ui/glassy/skin/glassy-ui.json");


        TextButton playOnlineButton = new TextButton("Play Online", style);
        playOnlineButton.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                game.isOnline = true;
                game.setScreen(new PlayerNameScreen(game));

                dispose();
            }
        });
        TextButton playButton = new TextButton("Play", style);
        playButton.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {

                game.isOnline = false;
                game.setScreen(new GameScreen(game));

                dispose();
            }
        });

        TextButton scoreButton = new TextButton("Score", style);
        scoreButton.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {

                game.setScreen(new ScoreScreen(game));

                dispose();
            }
        });

        TextButton quitButton = new TextButton("Quit", style);
        quitButton.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {

               System.exit(0);

                dispose();
            }
        });
        //Anadir los componentes a la tabla

        table.row();
        table.add(playButton).center().width(350).height(120).pad(10);

        table.row();
        table.add(playOnlineButton).center().width(350).height(120).pad(10);

        table.row();
        table.add(scoreButton).center().width(350).height(120).pad(10);

        table.row();
        table.add(quitButton).center().width(350).height(120).pad(10);

        Gdx.input.setInputProcessor(stage);
    }

    /**
     * Color de fondo y se pinta el escenario
     * @param delta tiempo entre cada frame que se renderiza
     */
    @Override
    public void render(float delta) {
        stage.act();
        Gdx.gl.glClearColor(0.7f, 0.4f, 1, 1);
       	Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
       	batch.begin();
       	batch.end();
        game.batch.setProjectionMatrix(stage.getCamera().combined);
        stage.draw();
    }

    /**
     * Actualiza el tamano de la ventana
     * @param width ancho
     * @param height alto
     */
    @Override
    public void resize(int width, int height) {
    port.update(width, height);
    }

    @Override
    public void pause() {


    }

    @Override
    public void resume() {


    }

    @Override
    public void hide() {


    }

    /**
     * Dispone el escenario
     */
    @Override
    public void dispose() {

        stage.dispose();
    }

}
