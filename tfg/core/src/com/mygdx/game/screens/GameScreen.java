package com.mygdx.game.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.World;
import com.mygdx.game.MyGdxGame;
import com.mygdx.game.characters.bullets.Shoot;
import com.mygdx.game.managers.CameraManager;
import com.mygdx.game.managers.KeyboardManager;
import com.mygdx.game.managers.LevelManager;
import com.mygdx.game.managers.ResourceManager;
import com.mygdx.game.util.GameStates;

/**
 * Clase en la que se renderizan
 * los limites de las colisiones
 *
 */
public class GameScreen implements Screen {

	public Hud hud;
	public OrthogonalTiledMapRenderer mapRender;
	public final World world;
	public final MyGdxGame game;
	public final LevelManager levelManager;
	//public final Box2DDebugRenderer box2dDeBugRender; //Limites de las colisiones
	public final CameraManager cameraManager;
	private KeyboardManager keyboardManager;
	private InputMultiplexer multiplexer;
	public boolean nextLevel = false;

	public GameStates state= GameStates.RUN;

	/**
	 * Constructor donde se inicializan las
	 * variables y se da gravedad al mundo
	 * @param game se pasa la variable de la clase incial
	 */
	public GameScreen (final MyGdxGame game) {
		
		//Gravedad del mundo
		world = new World(new Vector2(0, -10), true);
		this.game = game;
		this.levelManager = new LevelManager(this);
	//	this.box2dDeBugRender = new Box2DDebugRenderer();
		this.hud = new Hud(game, this);
		this.cameraManager = new CameraManager(levelManager);
		this.cameraManager.camera.position.set(cameraManager.viewPort.getWorldWidth()/2, cameraManager.viewPort.getWorldHeight()/2, 0);
		keyboardManager = new KeyboardManager(this);
		multiplexer = new InputMultiplexer();
		multiplexer.addProcessor(keyboardManager);
		Gdx.input.setInputProcessor(multiplexer);
		

	}

	@Override
	public void show() {

		
	}

	/**
	 * Renderiza los mapas, combina el menu
	 * con lo demas que aparece en la pantalla,
	 * tambien esta el boton pause,
	 * para parar el mundo, almacena las variables
	 * de puntos y vidas al cambiar de nivel
	 * @param delta tiempo entre cada frame que se renderiza
	 */
	@Override
	public void render(float delta) {

		if(state != GameStates.PAUSE) {
			levelManager.update(delta);
			world.step(Gdx.graphics.getDeltaTime(), 1, 1);
		}
		hud.update(delta);

		

		mapRender.setView(cameraManager.camera);
		cameraManager.camera.update();
		
		Gdx.gl.glClearColor(levelManager.color.r,levelManager.color.g, levelManager.color.b, levelManager.color.a);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		mapRender.render();
		game.batch.setProjectionMatrix(cameraManager.camera.combined);
		game.batch.begin();
		
		levelManager.render(delta, game.batch);
		game.batch.end();
		
		//Combina el menu Hud con el resto de lo que aparece en la pantalla
		game.batch.setProjectionMatrix(hud.stage.getCamera().combined);
		hud.stage.draw();
		levelManager.postrender(delta, game.batch);
	//	box2dDeBugRender.render(world, cameraManager.camera.combined);


		if(levelManager.player.lifes == 0) {

			game.setScreen(new GameOverScreen(game,levelManager.player.coin));
			dispose();
		}

		if(nextLevel){
			ResourceManager.lifes = levelManager.player.lifes;
			ResourceManager.coins = levelManager.player.coin;
			game.level++;
			game.setScreen(new GameScreen(game));
			this.dispose();
		}
	}


	/**
	 * Redimensiona y actualiza la camara
	 * @param width ancho
	 * @param height alto
	 */
	@Override
	public void resize(int width, int height) {
		cameraManager.viewPort.update(width, height);
		
	}

	/**
	 * Pausa el juego completo
	 */
	@Override
	public void pause() {

		state = GameStates.PAUSE;
	}

	/**
	 * Vuelve a accionar el juego
	 */
	@Override
	public void resume() {

		state = GameStates.RUN;
	}

	@Override
	public void hide() {

		
	}

	/**
	 * Para redisponer to do el nivel
	 */
	@Override
	public void dispose() {

		world.dispose();
		hud.dispose();
		mapRender.dispose();
	//	box2dDeBugRender.dispose();
		levelManager.dispose();
	}

}
