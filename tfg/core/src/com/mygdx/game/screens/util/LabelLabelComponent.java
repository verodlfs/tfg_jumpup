package com.mygdx.game.screens.util;

import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

public class LabelLabelComponent {
    public Table table;
    public Label label;
    public Label value;

    /**
     * Clase para crear etiquetas
     * @param labelText etiqueta de texto
     * @param valueText comentario
     * @param table crear una tabla
     * @param skin le pasa una estilo
     */
    public LabelLabelComponent(String labelText, String valueText, Table table, Skin skin){
        this.table = table;
        this.label = new Label(labelText, skin);
        this.value = new Label(valueText, skin);
        table.row().pad(5).center();
        table.add(label).right();
        table.add(value).left();
    }
}
