package com.mygdx.game.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.mygdx.game.MyGdxGame;
import com.mygdx.game.managers.ResourceManager;
import com.mygdx.game.servidor.Client;
import com.mygdx.game.servidor.dto.PlayerDataDTO;
import com.mygdx.game.servidor.dto.ScoreDTO;
import com.mygdx.game.util.Constants;
//import vero.datos.UsuarioDTO;

/**
 * Clase de fin de juego por haber perdido
 */
public class GameOverScreen implements Screen{

	final MyGdxGame game;
	private Stage stage;
	private Table table;
	private int score;
	private Client client;
	private boolean anotherGame=false;
	private boolean optionSelected=false;
	private Texture texture;
	private SpriteBatch batch;
	private Viewport port;


	public GameOverScreen(final MyGdxGame game, int score) {
		this.game = game;
		this.score = score;
		client = new Client();

	}

	/**
	 * Reseta el contador de los puntos y vidas,
	 * y pregunta si se quiere volver a jugar,
	 * dependiendo del boton que pulse
	 * se realizara una funcion u otra
	 */
	@Override
	public void show() {

		ResourceManager.lifes = 1;
		ResourceManager.coins = 0;

		batch = new SpriteBatch();
		texture = new Texture(Gdx.files.internal("textures/images/playagain.png"));

		stage = new Stage();
		this.port = new FitViewport(Constants.SCREEN_WIDTH,Constants.SCREEN_HEIGHT,new OrthographicCamera());
		stage = new Stage(port);
		Gdx.input.setInputProcessor(stage);
		table = new Table();

		table.setPosition(Constants.SCREEN_WIDTH-950, 500f);

		stage.addActor(new Image(texture));
		stage.addActor(table);


		Skin style = ResourceManager.getSkin("ui/glassy/skin/glassy-ui.json");

		

		TextButton playButton = new TextButton("YES", style);
		playButton.addListener(new ClickListener() {
			public void clicked(InputEvent event, float x, float y) {
				game.level=1;
				optionSelected = true;
				anotherGame = true;

			}
		});

		TextButton optionsButton = new TextButton("NO", style);
		optionsButton.addListener(new ClickListener() {
			public void clicked(InputEvent event, float x, float y) {
				game.level=1;
				optionSelected = true;
				anotherGame = false;

			}
		});
		//Anadir los componentes a la tabla

		table.row();
		table.add(playButton).center().width(250).height(120).pad(5);

		table.row();
		table.add(optionsButton).center().width(350).height(120).pad(5);

		
		Gdx.input.setInputProcessor(stage);

		if(game.isOnline) {
			client.sendData(new PlayerDataDTO(ResourceManager.userName, score));
		}

		ResourceManager.scores.add(new ScoreDTO( game.level,this.score));
		ResourceManager.saveScore();
	}

	@Override
	public void render(float delta) {
		//Comprobar que se han enviado los datos al servidor.
		if(!client.isRunning){

			if(optionSelected){
				if(anotherGame) {
					game.setScreen(new GameScreen(game));
					dispose();
				}else{
					anotherGame = true;
					//en el caso de que no seleccione no se hace nada
					if(anotherGame){

						game.setScreen(new MenuScreen(game));
						dispose();
						anotherGame = false;
					}
				}
			}

		}else{
			if(optionSelected) {
				if(anotherGame) {
					game.setScreen(new PlayerNameScreen(game));
					dispose();
				}else{
					anotherGame = true;
					if(anotherGame){
						game.setScreen(new MenuScreen(game));
						anotherGame = false;
					}
				}
			}
		}

		stage.act(delta);
		Gdx.gl.glClearColor(0.7f, 0.4f, 1, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		batch.begin();
		batch.end();
		game.batch.setProjectionMatrix(stage.getCamera().combined);
		stage.draw();
	}

	/**
	 * Tamano de la pantalla
	 * @param width ancho
	 * @param height alto
	 */
	@Override
	public void resize(int width, int height) {

		port.update(width, height);
		
	}

	@Override
	public void pause() {

		
	}

	@Override
	public void resume() {

		
	}

	@Override
	public void hide() {

		
	}

	/**
	 * Se recoloca el escenario
	 */
	@Override
	public void dispose() {

		stage.dispose();
	}

}
