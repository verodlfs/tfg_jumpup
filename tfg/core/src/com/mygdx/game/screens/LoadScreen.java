package com.mygdx.game.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.mygdx.game.MyGdxGame;
import com.mygdx.game.managers.ResourceManager;
import com.mygdx.game.util.Constants;

/**
 * Pantalla de carga inicial
 */
public class LoadScreen implements Screen{
	
	private Texture loadTexture;
	private OrthographicCamera camera;
	
	private Viewport viewPort;
	private Image image;
	private Stage stage;
	
	final MyGdxGame game;
	
	private boolean nextScreen = false;

	/**
	 * Se inicializan las variables y se carga la imagen
	 * @param game MyGdxGame
	 */
	public LoadScreen(final MyGdxGame game) {

		  this.game = game;
		  //Cargar la imagen del load
		  loadTexture = new Texture(Gdx.files.internal("textures/images/logo.jpg"));
		  image = new Image(loadTexture);
		  image.setPosition(180, 150);
		  image.setDrawable(new TextureRegionDrawable(loadTexture));
		  image.setSize(350, 300);
		  
		  camera = new OrthographicCamera();
		  viewPort = new FitViewport(700, 700);
		  stage = new Stage(viewPort);
		  stage.addActor(image);
		  
	}


	/**
	 * Muestra la imagen durante unos segundos, y le da animacion de aparecer
	 * cunado entre en el run cambiara  a la siguiente pantalla
	 */
	@Override
	public void show() {

		//Muestra la imagen durante unos segundos, y le da animacion de aparecer.
		image.addAction(Actions.sequence(Actions.alpha(0), Actions.fadeIn(1f), Actions.delay(1f), Actions.run(new Runnable() {

			@Override
			public void run() {
				//Cuando entre cambiara a la siguiente pantalla
				nextScreen = true;	
			}
			
		})));
		
		ResourceManager.loadAllResources();
		ResourceManager.loadScore();
		
		
	}

	/**
	 * Se renderiza lo que aparecia anteriormente
	 * @param delta tiempo entre cada frame que se renderiza
	 */
	@Override
	public void render(float delta) {
		
		update(delta);
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		game.batch.setProjectionMatrix(camera.combined);
		game.batch.begin();
		
		//Tapa lo que aparecia anteriormente
		game.batch.disableBlending();
		
		stage.act();
		stage.draw();
		game.batch.enableBlending();
		game.batch.end();
		
	}

	/**
	 * Se actualiza la ventana
	 * @param width ancho
	 * @param height alto
	 */
	@Override
	public void resize(int width, int height) {

		viewPort.update(width, height);
		
	}

	@Override
	public void pause() {

		
	}

	@Override
	public void resume() {

		
	}

	@Override
	public void hide() {

		
	}

	/**
	 * Se dispone la textura de carga
	 * y el escenario donde estaran todos
	 * los elementos
	 */
	@Override
	public void dispose() {
		
		loadTexture.dispose();
		stage.dispose();
		
	}

	/**
	 * La siguientte pantalla que se mostrará después de
	 * la imagen de carga
	 * @param dt tiempo entre cada frame que se renderiza
	 */
	private void update(float dt) {
	
		if(ResourceManager.update()) {
			if(nextScreen) {
				game.setScreen(new MenuScreen(game));
				this.dispose();
			}
		}
	}
	
	

}
