package com.mygdx.game.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.mygdx.game.MyGdxGame;
import com.mygdx.game.managers.ResourceManager;
import com.mygdx.game.screens.util.LabelLabelComponent;
import com.mygdx.game.servidor.dto.ScoreDTO;

/**
 * Clase de que ha ganado el juego
 */
public class WinScreen implements Screen {

    final MyGdxGame game;
    private Stage stage;
    private Table table;
    private Texture texture;
    private SpriteBatch batch;

    /**
     * Constructor
     * @param game MyGdxGame
     */
    public WinScreen(final MyGdxGame game) {
        this.game = game;
    }

    /**
     * Muestra la imagen de fondo,
     * carga los estilos, y los botones correspondientes...
     */
    @Override
    public void show() {
        batch = new SpriteBatch();
        texture = new Texture(Gdx.files.internal("textures/images/ganar.jpg"));

        stage = new Stage();

        stage = new Stage();
        Gdx.input.setInputProcessor(stage);
        table = new Table();

        table.setFillParent(true);
        table.setPosition(0,-400f);
        stage.addActor(table);


        Skin style = ResourceManager.getSkin("ui/glassy/skin/glassy-ui.json");

        TextButton backButton = new TextButton("Play Again", style);
        backButton.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {

                game.setScreen(new MenuScreen(game));

                dispose();
            }
        });



        table.row();
        table.add(backButton).center().width(350).height(120).pad(10);

    }

    /**
     * Renderiza el color de fondo y el escenario
     * @param delta tiempo entre cada frame que se renderiza
     */
    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0.7f, 0.4f, 1, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        batch.begin();
        batch.draw(texture, 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        batch.end();

        stage.act();
        stage.draw();
    }

    /**
     * Actualiza el tamano de la ventana
     * @param width ancho
     * @param height alto
     */
    @Override
    public void resize(int width, int height) {
        stage.getViewport().update(width, height);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    /**
     * Dispone el escenario
     */
    @Override
    public void dispose() {
        stage.dispose();
    }
}
