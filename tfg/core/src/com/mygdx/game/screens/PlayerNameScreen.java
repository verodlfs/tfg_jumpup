package com.mygdx.game.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.*;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.mygdx.game.MyGdxGame;
import com.mygdx.game.managers.ResourceManager;
import com.mygdx.game.servidor.Client;

public class PlayerNameScreen implements Screen {

	public final MyGdxGame game;
	private Stage stage;
	private Table table;
	private int currentLevel = 1;
	public TextField txtName;

	public PlayerNameScreen(final MyGdxGame game) {

		this.game = game;
	}

	@Override
	public void show() {
		stage = new Stage();

		stage = new Stage();
		Gdx.input.setInputProcessor(stage);
		table = new Table();

		table.setFillParent(true);
		stage.addActor(table);

		Skin style = ResourceManager.getSkin("ui/glassy/skin/glassy-ui.json");
		Label title = new Label("Introduce tu nombre de Jugador: ", style);
		txtName = new TextField("", style);

		TextButton enterButton = new TextButton("Entrar", style);
		enterButton.addListener(new ClickListener() {
			public void clicked(InputEvent event, float x, float y) {

				ResourceManager.userName = txtName.getText();
				game.isOnline = true;

				game.setScreen(new GameScreen(game));

				dispose();
			}
		});
		
		table.row();
		table.add(title).center().width(300).height(120).pad(5);
		
		table.row();
		table.add(txtName).center().width(300).height(50).pad(3);
		
		table.row();
		table.add(enterButton).center().width(250).height(100).pad(5);
		
		Gdx.input.setInputProcessor(stage);

	}

	@Override
	public void render(float delta) {

		Gdx.gl.glClearColor(0.7f, 0.4f, 1, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		stage.act(delta);
		stage.draw();

	}

	@Override
	public void resize(int width, int height) {
		stage.getViewport().update(width, height);
	}

	@Override
	public void pause() {


	}

	@Override
	public void resume() {


	}

	@Override
	public void hide() {


	}

	@Override
	public void dispose() {
		stage.dispose();

	}

}
