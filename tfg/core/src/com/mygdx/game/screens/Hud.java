package com.mygdx.game.screens;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.mygdx.game.MyGdxGame;
import com.mygdx.game.managers.ResourceManager;
import com.mygdx.game.util.Constants;

/**
 * Menu implementado en los niveles
 */
public class Hud implements Disposable{
	
	final GameScreen gameScreen;
	final MyGdxGame game;
	public Stage stage;
	protected Viewport viewPort;
	Label life;
	Label money;
	Label shield;
	Label ultrashield;
	Label namePlayer;
	public PlayerNameScreen name;
	static final float fontScale = 0.75f;

	/**
	 * Inicializa las variables,
	 * y se crea el menu
	 * @param game MyGdxGame
	 * @param gameScreen GameScreen
	 */
	public Hud(final MyGdxGame game, final GameScreen gameScreen) {
		this.gameScreen = gameScreen;
		viewPort = new FitViewport(Constants.SCREEN_WIDTH, Constants.SCREEN_HEIGHT, new OrthographicCamera());
		stage = new Stage(viewPort);
		this.game = game;

		Skin style = ResourceManager.getSkin("ui/glassy/skin/glassy-ui.json");

		life = new Label("LIFES", style);
		life.setFontScale(fontScale);

		money = new Label("COINS", style);
		money.setFontScale(fontScale);

		shield = new Label("SHIELD", style);
		shield.setFontScale(fontScale);

		ultrashield = new Label("ULTRA SHIELD", style);
		ultrashield.setFontScale(fontScale);

		namePlayer = new Label("PLAYER: " + ResourceManager.userName, style);
		namePlayer.setFontScale(fontScale);

		Table table = new Table();
		table.setFillParent(false);

		table.setPosition(0, Constants.SCREEN_HEIGHT);

		table.row().height(30);
		table.add(life).width(100);

		table.row().height(30);
		table.add(money).width(100);

		table.row().height(30);
		table.add(shield).width(100);

		table.row().height(30);
		table.add(ultrashield).width(100);


		if (game.isOnline){
			table.row().height(30);
			table.add(namePlayer).width(100);
		}


		table.setPosition(80, Constants.SCREEN_HEIGHT-30-(table.getRows()*30));
		stage.addActor(table);


		
	}

	/**
	 * Se establece el escenario
	 */
	@Override
	public void dispose() {
		stage.dispose();
	}
	
	public void update(float delta) {

	}

	/**
	 * Se actualizan las monedas conseguidas
	 * @param coin se pasa la puntuacion de las monedas
	 *                y se muestra en el hud
	 */
	public void updateCoin(int coin){
		money.setText("COINS: " + coin);
	}

	/**
	 * Se actualizan las vidas conseguidas
	 * @param lifes se pasa el recuento de todas las vidas
	 *                y se muestra en el hud
	 */
	public void updateLife(int lifes){
		life.setText("LIFES: " + lifes);
		if(lifes < 0 ){
			life.setText("LIFES: " + 0);

		}
	}

	/**
	 *  Se actualizan los escudos conseguidas
	 * @param shields se realiza una cuenta atras de tiempo
	 */
	public void updateShield(int shields){
		shield.setText("SHIELD: " + shields);

	}

	/**
	 *  Se actualizan los ultra escudos conseguidas
	 * @param ultraShield se realiza una cuenta atras de tiempo
	 */
    public void updateUltrashield(int ultraShield) {
		ultrashield.setText("ULTRASHIELD: " + ultraShield);
    }
}
