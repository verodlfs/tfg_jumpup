package com.mygdx.game.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.mygdx.game.MyGdxGame;
import com.mygdx.game.managers.ResourceManager;
import com.mygdx.game.screens.util.LabelLabelComponent;
import com.mygdx.game.servidor.dto.PlayerDataDTO;
import com.mygdx.game.servidor.dto.ScoreDTO;
import com.mygdx.game.util.Constants;

/**
 * Clase que muestra las ultimas 5 puntuaciones
 */
public class ScoreScreen implements Screen {

    final MyGdxGame game;
    private Stage stage;
    private Table table;
    private Texture texture;
    private SpriteBatch batch;
    private Viewport port;

    /**
     * Constructor
     * @param game MyGdxGame
     */
    public ScoreScreen(final MyGdxGame game) {
        this.game = game;
    }

    /**
     * Carga imagen de fondo, botones necesarios,
     * borra y sobreescribe las puntuaciones
     */
    @Override
    public void show() {
        batch = new SpriteBatch();
        texture = new Texture(Gdx.files.internal("textures/images/score.png"));

        stage = new Stage();
        this.port = new FitViewport(Constants.SCREEN_WIDTH,Constants.SCREEN_HEIGHT,new OrthographicCamera());
        stage = new Stage(port);
        Gdx.input.setInputProcessor(stage);
        table = new Table();

        table.setPosition(Constants.SCREEN_WIDTH-1000, 500f);

        stage.addActor(new Image(texture));
        stage.addActor(table);


        Skin style = ResourceManager.getSkin("ui/glassy/skin/glassy-ui.json");

        TextButton backButton = new TextButton("Go back", style);
        backButton.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {

                game.setScreen(new MenuScreen(game));

                dispose();
            }
        });

        System.out.println(ResourceManager.scores.size);

        if (ResourceManager.scores.size >= 6){
            //Para borrar los primeros registros, los mas antiguos
                ResourceManager.scores.removeRange(0, ResourceManager.scores.size - 6);
        }
        System.out.println(ResourceManager.scores.size);
        for (ScoreDTO data: ResourceManager.scores) {
           new LabelLabelComponent("Score:", data.playerScore + "", table, style);
        }



        table.row();
        table.add(backButton).center().width(350).height(120).pad(10).colspan(2);

    }

    /**
     * Renderiza el color de fondo y el escenario
     * @param delta tiempo entre cada frame que se renderiza
     */
    @Override
    public void render(float delta) {
        stage.act();
        Gdx.gl.glClearColor(0.7f, 0.4f, 1, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        batch.begin();
        batch.end();
        game.batch.setProjectionMatrix(stage.getCamera().combined);
        stage.draw();
    }

    /**
     * Actualiza el tamano de la ventana
     * @param width ancho
     * @param height alto
     */
    @Override
    public void resize(int width, int height) {
        port.update(width, height);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    /**
     * Dispone el escenario
     */
    @Override
    public void dispose() {
        stage.dispose();
    }
}
