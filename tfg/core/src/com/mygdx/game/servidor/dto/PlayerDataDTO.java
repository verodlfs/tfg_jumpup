package com.mygdx.game.servidor.dto;

import java.io.Serializable;

public class PlayerDataDTO implements Serializable {
    private String nombre;
    private int score;
    //Para no tener problemas a la hora de serializar esta clase y tiene que ser identica en todos los proyectos
    // (el codigo tbn tiene que ser identico con los mismos valores)
    public static final long serialVersionUID = 1L;

    public PlayerDataDTO(int score) {
        this.score = score;
        this.nombre = "";
    }

    public PlayerDataDTO(String nombre, int score) {
        this.nombre = nombre;
        this.score = score;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }
}

