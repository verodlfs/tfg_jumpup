package com.mygdx.game.servidor.dto;

import com.badlogic.gdx.utils.Array;

public class ScoreDTO {

    public int playerScore;
    public int level;

    public ScoreDTO(){
    }

    public ScoreDTO(int playerScore, int level) {
        this.playerScore = playerScore;
        this.level = level;
    }
}
