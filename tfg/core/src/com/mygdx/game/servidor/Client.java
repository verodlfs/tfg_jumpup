package com.mygdx.game.servidor;


import com.mygdx.game.servidor.dto.PlayerDataDTO;


import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.security.Key;

public class Client implements Runnable {

    Socket socketCliente;
    private PlayerDataDTO data;
    private static Key clave;

    public Thread thread;
    public boolean isRunning = false;

    private synchronized void setRunning(boolean running) {
        this.isRunning = running;
    }

    public void sendData(PlayerDataDTO data) {
        this.data = data;
        thread = new Thread(this);
        thread.start();
    }

    @Override
    public void run() {
        //Paso el localhost
        setRunning(true);
        InetAddress host = null;
        try {
            host = InetAddress.getLocalHost();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }

        //Creo el socket del cliente
        socketCliente = new Socket();
        try {
            socketCliente = new Socket(host.getHostName(), 9876);

            ObjectOutputStream oos = new ObjectOutputStream(socketCliente.getOutputStream());
            ObjectInputStream ois = new ObjectInputStream(socketCliente.getInputStream());

                //Añadir el cifrado parte cliente
                clave = (Key) ois.readObject();
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            ObjectOutputStream out = null;
            byte[] request= null;
            try {
                out = new ObjectOutputStream(bos);
                out.writeObject(data);
                out.flush();
                request= bos.toByteArray();
            }catch (Exception e){
                System.err.println(e.getMessage());
            } finally {
                try {
                    bos.close();
                } catch (IOException ex) {

                }
            }
                //para que pinte la informacion
            byte[] gf = vero.encriptar.Encriptacion.encriptar(request, clave);
                oos.writeObject(gf);
                oos.flush();

                Thread.sleep(1000);

                oos.close();
                ois.close();
                socketCliente.close();

            } catch (IOException | InterruptedException | ClassNotFoundException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
            setRunning(false);
        }

}

