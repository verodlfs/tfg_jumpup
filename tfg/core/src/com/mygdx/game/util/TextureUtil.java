package com.mygdx.game.util;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

/**
 * Clase para cargar las texturas en cada clase
 */
public class TextureUtil {

	public static TextureRegion getFrame(float dt, Animation<TextureRegion> animation) {
		return animation.getKeyFrame(dt, true);
		
	}
}
