package com.mygdx.game.util;

/**
 * Clase donde se encuentran todas las
 * constantes con los bits de colision,
 * dar tamano de forma al cuerpo,
 * mover la imagen, sensores de colision...
 */
public class Constants {
	
	public static final int SCREEN_WIDTH = 1920;
	public static final int SCREEN_HEIGHT = 1080;
	
	//Para escalar el juego
	public static final float PIXELES_MM = 150;
	public static final float PLAYER_SCALE = 1.25f;
	public static final float ENEMY_BASE_SALE = 1f;
	public static final float COIN_SCALE = 2f;
	public static final float SHIELD_SCALE = 2f;
	public static final float LIFE_SCALE = 2f;
	
	//tamano del forma del cuerpo (el circulo que colisiona)
	public static final float PLAYER_BODY_SCALE = 1f;
	public static final float PLAYER_HEAD_SCALE = 0.5f;
	public static final float ENEMY_BODY_SCALE = 1f;
	public static final float COIN_BODY_SCALE = 1.2f;
	public static final float SHIELD_BODY_SCALE = 1.5f;
	public static final float LIFE_BODY_SCALE = 1f;

	public static final float SHIELD_SCALE_PLAYER = 0.75f;
	
	//mover la imagen del jugador relativa al cuerpo que colisiona
	public static final float  PLAYER_OFFSET_SPRITE_X = 0.1f;
	public static final float  PLAYER_OFFSET_SPRITE_Y = 0.1f;
	
	public static final float  ENEMY_OFFSET_SPRITE_X = 0f;
	public static final float  ENEMY_OFFSET_SPRITE_Y = 0;
	
	//bits para los diferentes objetos(colisiones)
	public static final short PLAYER = 2;
	public static final short PLAYER_HEAD = 256;

	public static final short PLATFORM = 1;
	
	public static final short ENEMY = 4;

	public static final short COIN = 8;

	public static final short LIFE = 16;

	public static final short SHIELD = 1024;

	public static final short ULTRA_SHIELD = 4096;

	public static final short NEXT_LEVEL = 128;

	public static final short STAR = 16384;

	public static final short MOVE_PLATFORM = 2048;

	//Sensor de colision del enemigo 
	public static final short SENSOR_LEFT_ENEMY = 32;
	public static final short SENSOR_RIGHT_ENEMY = 64;

	public static final short BULLET = 8192;

}
