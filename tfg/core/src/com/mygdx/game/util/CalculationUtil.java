package com.mygdx.game.util;

import com.badlogic.gdx.physics.box2d.Body;
import com.mygdx.game.screens.GameScreen;

/**
 * Clase que calcula el rango
 */
public class CalculationUtil {
    /**
     * Calcula el rango en el que esta el jugador
     * para que el enemigo volador pueda
     * acudir a ese rango y atacar al jugador
     * @param body1 indica donde se encuentra el jugador
     * @param body2 indica donde se encuentra el enemigo volador
     * @param range rango en el que se encuentran los dos objetos
     * @return distance mayor que -range y distance menor que range devuelve la distancia obtenida
     */
    public static boolean calculateRange(Body body1, Body body2, float range) {

        double playerX = body1.getPosition().x;
        double playerY = body1.getPosition().y;

        double ghostX = body2.getPosition().x;
        double ghostY = body2.getPosition().y;

        double distance = Math.sqrt(Math.pow (ghostX - playerX,2) + Math.pow(ghostY - playerY, 2));

        return distance > -range && distance < range;
    }
}
