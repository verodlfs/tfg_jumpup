package com.mygdx.game.util;

/**
 * Clase enumeracion de las diferentes animaciones
 * que tienen los jugadores, enemigos...
 */
public enum Animations {
    WALK_LEFT, WALK_RIGHT, JUMP_LEFT, JUMP_RIGHT, ATTACK, IDLE
}
