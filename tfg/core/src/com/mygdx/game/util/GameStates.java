package com.mygdx.game.util;

/**
 * Clase enumeracion que contiene
 * los estados en los que se va
 * a poder encontrar el juego
 */
public enum GameStates {
    PAUSE,
    RUN
}
