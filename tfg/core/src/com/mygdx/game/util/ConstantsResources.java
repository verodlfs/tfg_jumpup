package com.mygdx.game.util;

/**
 * Clase donde se cargan todas
 * las texturas y sonidos que
 * se van a emplear
 */
public class ConstantsResources {

    public static final String PLAYER = "textures/character/Player/Jugador.atlas";

    //LEVEL - 1
    public static final String[] ENEMY = new String[]{
            "textures/character/Enemy/Momia.atlas",//0
            "textures/character/Enemy2/Frank.atlas",//1
            "textures/character/Enemy3/Esqueleto.atlas",//2
            "textures/character/Enemy4/Momiamarilla.atlas",//3
            "textures/character/Enemy5/Momiarosa.atlas",//4
            "textures/character/Enemy6/Policia.atlas",//5
            "textures/character/Enemy7/Zombie.atlas",//6

            //LEVEL - 2
            "textures/character/Enemy8/Dinosaurio1.atlas",//7
            "textures/character/Enemy9/Dinosaurio2.atlas",//8
            "textures/character/Enemy10/Dinosaurio3.atlas",//9
            "textures/character/Enemy11/Dinosaurio4.atlas",//10
            "textures/character/Enemy12/Granjero1.atlas",//11
            "textures/character/Enemy13/Granjero2.atlas",//12
            "textures/character/Pajaro/pajaro.atlas",//13

            //LEVEL - 3
            "textures/character/Enemy14/mago.atlas",//14
            "textures/character/Enemy15/bruja.atlas",//15
            "textures/character/Enemy16/piedra.atlas",//16
            "textures/character/Enemy17/vikingo.atlas",//17
            "textures/character/Enemy18/arquero.atlas",//18
            "textures/character/Enemy19/bruja2.atlas"//19
    };


    public static final String COIN = "textures/images/coin/coin.atlas";
    public static final String SHIELD = "textures/images/escudo/escudo.atlas";
    public static final String SHIELD_TEXTURE = "textures/images/escudo/escudo.png";
    public static final String ULTRASHIELD = "textures/images/escudo/ultraescudo.atlas";
    public static final String ULTRASHIELD_TEXTURE = "textures/images/escudo/ultraescudo.png";
    public static final String LIFE = "textures/images/life/life.atlas";
    public static final String BULLET = "textures/images/bala/bala.atlas";
    public static final String DOOR_NEXTLEVEL = "textures/images/nextLevel/puertaNextLevel.png";
    public static final String STAR = "textures/images/star/star.png";
    public static final String POWER_UP = "textures/images/powerup/powerup.png";
    public static final String TORRE = "textures/images/torre.png" ;


    public static final String MENU_SKIN = "ui/glassy/skin/glassy-ui.json";

    public static final String SOUND_COIN = "sounds/coin.ogg";
    public static final String SOUND_GHOST = "sounds/ghost.ogg";
    public static final String SOUND_LIFE = "sounds/life.ogg";
    public static final String SOUND_SHIELD = "sounds/shield.ogg";
    public static final String SOUND_SHOOT = "sounds/shoot.ogg";
    public static final String SOUND_WIN = "sounds/win.ogg";
    public static final String SOUND_DOOR = "sounds/puerta.ogg";

    public static final String[] MOVE_PLATFORM = new String[]{
            "textures/images/platform/move_platform.atlas"
    };



}
