package com.mygdx.game.util;

/**
 * Clase enumeracion que contiene
 * todas las direcciones en las
 * que se van a poder mover todos
 * los elementos dinamicos
 */
public enum Directions {
    IDLE,
    UP,
    DOWN,
    LEFT,
    RIGHT
}
