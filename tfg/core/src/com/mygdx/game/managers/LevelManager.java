package com.mygdx.game.managers;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.Array;
import com.mygdx.game.characters.*;
import com.mygdx.game.characters.enemies.Enemy;
import com.mygdx.game.characters.enemies.EnemyJump;
import com.mygdx.game.characters.enemies.Ghost;
import com.mygdx.game.characters.objects.*;
import com.mygdx.game.characters.player.Player;
import com.mygdx.game.screens.GameScreen;
import com.mygdx.game.screens.MenuScreen;
import com.mygdx.game.util.Constants;

/**
 * Clase en la que se cargan las colisiones, todas las texturas...
 */
public class LevelManager {

    public TiledMap map;
    private GameScreen gameScreen;
    public Player player;
    public Array<DynamicElement> enemies;
    public Array<MovePlatform> platforms;
    public Array<Coin> coins = new Array<>();
    public Array<Life> lifes = new Array<>();
    public Array<Shield> shields = new Array<>();
    public Array<Ultrashield> ultraShields = new Array<>();
    public NextLevel nextLevel;
    public NextLevel finalLevel;
    public PowerUpNextLevel powerUp;
    public Star winScreen;
    public Color color;

    /**
     * Se inicializan las variables
     * @param game para poder acceder a sus metodos y variables
     */
    public LevelManager(GameScreen game) {
        this.gameScreen = game;
        enemies = new Array<>();
        platforms = new Array<>();
        loadMap();
    }

    /**
     * Metodo para cargar el mapa
     * y para detectar todas las colisiones
     */
    private void loadMap() {

        map = new TmxMapLoader().load("maps/level" + gameScreen.game.level + "/level" + gameScreen.game.level + ".tmx");
        gameScreen.mapRender = new OrthogonalTiledMapRenderer(map, 1 / Constants.PIXELES_MM);

        //Detectar las colisiones
        gameScreen.world.setContactListener(new CollisionManager());

        if (map.getLayers().get("Colision") != null) {
            //El suelo, para coger la colision
            for (MapObject object : map.getLayers().get("Colision").getObjects().getByType(RectangleMapObject.class)) {
                Rectangle rect = ((RectangleMapObject) object).getRectangle();
                new Platform(rect, gameScreen, Constants.PLATFORM);

            }
        }

        if (map.getLayers().get("Coin") != null) {
            for (MapObject point : map.getLayers().get("Coin").getObjects().getByType(RectangleMapObject.class)) {
                Rectangle rectCoin = ((RectangleMapObject) point).getRectangle();
                coins.add(new Coin(map, rectCoin, gameScreen.world));

            }
        }

        if (map.getLayers().get("Shield") != null) {
            for (MapObject shield : map.getLayers().get("Shield").getObjects().getByType(RectangleMapObject.class)) {
                Rectangle rectShield = ((RectangleMapObject) shield).getRectangle();
                shields.add(new Shield(map, rectShield, gameScreen.world));

            }
        }

        if (map.getLayers().get("Ultrashield") != null) {
            for (MapObject ultrashield : map.getLayers().get("Ultrashield").getObjects().getByType(RectangleMapObject.class)) {
                Rectangle rectUltrashield = ((RectangleMapObject) ultrashield).getRectangle();
                ultraShields.add(new Ultrashield(map, rectUltrashield, gameScreen.world));

            }
        }

        if (map.getLayers().get("LifeColision") != null) {
            for (MapObject life : map.getLayers().get("LifeColision").getObjects().getByType(RectangleMapObject.class)) {
                Rectangle rectLife = ((RectangleMapObject) life).getRectangle();
                lifes.add(new Life(map, rectLife, gameScreen.world));

            }

        }

        if (map.getLayers().get("Player") != null) {
            MapObject playerSpawn = map.getLayers().get("Player").getObjects().getByType(RectangleMapObject.class).first();
            Rectangle rect = ((RectangleMapObject) playerSpawn).getRectangle();
            player = new Player(map, rect, gameScreen.world, gameScreen);
            //Para dar color de fondo al nivel(desde tiled)
            if (playerSpawn.getProperties().containsKey("R") && playerSpawn.getProperties().containsKey("G") && playerSpawn.getProperties().containsKey("B")) {
                color = new Color();
                color.a = 1;
                color.r = (float) playerSpawn.getProperties().get("R");
                color.g = (float) playerSpawn.getProperties().get("G");
                color.b = (float) playerSpawn.getProperties().get("B");
            } else {
                color = Color.BLACK;
            }

        }

        //si es online y es el ultimo mapa se mostrará el fin de juego
        if ((gameScreen.game.isOnline && gameScreen.game.level == 5) || (!gameScreen.game.isOnline && gameScreen.game.level == 3)) {
            if (map.getLayers().get("WinScreen") != null ) {
                MapObject objectWinScreen = map.getLayers().get("WinScreen").getObjects().getByType(RectangleMapObject.class).first();
                Rectangle rectWinScreen = ((RectangleMapObject) objectWinScreen).getRectangle();
                winScreen = new Star(map, rectWinScreen, gameScreen.world, gameScreen.game);
            }
        } else {
            if (map.getLayers().get("ColisionNextLevel") != null) {
                MapObject objectNextLevel = map.getLayers().get("ColisionNextLevel").getObjects().getByType(RectangleMapObject.class).first();
                Rectangle rectNextLevel = ((RectangleMapObject) objectNextLevel).getRectangle();
                nextLevel = new NextLevel(rectNextLevel, gameScreen, Constants.NEXT_LEVEL);


            }
        }

        if (map.getLayers().get("PowerUpNextLevel") != null) {
            MapObject mapPowerUp = map.getLayers().get("PowerUpNextLevel").getObjects().getByType(RectangleMapObject.class).first();
            Rectangle rectPowerUp = ((RectangleMapObject) mapPowerUp).getRectangle();
            powerUp = new PowerUpNextLevel(rectPowerUp, gameScreen, Constants.NEXT_LEVEL);


        }

        if (map.getLayers().get("Enemy") != null) {
            for (MapObject enemySpawn : map.getLayers().get("Enemy").getObjects().getByType(RectangleMapObject.class)) {
                Rectangle rectEnemy = ((RectangleMapObject) enemySpawn).getRectangle();
                if (enemySpawn.getProperties().containsKey("enemyType")) {

                    switch ((int) enemySpawn.getProperties().get("enemyType")) {
                        case 0:
                            enemies.add(new Enemy(map, rectEnemy, gameScreen.world, (int) enemySpawn.getProperties().get("skin"), 2));
                            break;
                        case 1:
                            enemies.add(new Ghost(map, rectEnemy, gameScreen.world, (int) enemySpawn.getProperties()
                                    .get("skin"), gameScreen));
                            break;

                        case 2:
                            enemies.add(new EnemyJump(map, rectEnemy, gameScreen.world, (int) enemySpawn.getProperties().get("skin"), 2));
                            break;

                        case 3:
                            enemies.add(new Torre(map, rectEnemy, gameScreen.world, gameScreen));


                    }
                }
            }
        }

        if (map.getLayers().get("ColisionMovePlatform") != null) {
            //El suelo, para coger la colision
            for (MapObject movePlatform : map.getLayers().get("ColisionMovePlatform").getObjects().getByType(RectangleMapObject.class)) {
                Rectangle rectmovePlatform = ((RectangleMapObject) movePlatform).getRectangle();
                if (movePlatform.getProperties().containsKey("type")) {
                    switch ((int) movePlatform.getProperties().get("type")) {
                        case 0:
                            if (movePlatform.getProperties().containsKey("skin")) {
                                platforms.add(new MovePlatform(
                                        map,
                                        rectmovePlatform,
                                        gameScreen.world,
                                        (int) movePlatform.getProperties().get("skin"),
                                        1)
                                );
                            } else {
                                platforms.add(new MovePlatform(
                                        map,
                                        rectmovePlatform,
                                        gameScreen.world,
                                        0,
                                        1));
                            }
                            break;
                    }
                }
            }
        }
    }


    /**
     * Actualiza todas las entidades del nivel
     *
     * @param dt  tiempo entre cada frame que se renderiza
     */
    public void update(float dt) {

        if (player != null) {
            gameScreen.cameraManager.camera.position.set(player.body.getPosition().x, player.body.getPosition().y, 0);
            player.update(dt);
        }

        for (DynamicElement enemy : enemies) {
            enemy.update(dt);
            if (enemy.toDestroy) {
                //comprueba si el enemigo ha sido asesinado si es asi lo elimina
                enemy.destroyBody();
                enemies.removeValue(enemy, true);
            }
        }

        for (MovePlatform plat : platforms) {
            plat.update(dt);

        }

        if (player != null) {
            //coge las monedas/vidas/escudos... que tiene el jugador y los pinta en la pantalla
            gameScreen.hud.updateCoin(player.coin);
            gameScreen.hud.updateLife(player.lifes);
            gameScreen.hud.updateShield(player.shield);
            gameScreen.hud.updateUltrashield(player.ultraShield);
            gameScreen.cameraManager.update(dt, player);
        }

        for (Coin c : coins) {
            c.update(dt);
            if (c.toDestroy) {
                //comprueba si la moneda ha sido recogida si es asi la elimina
                c.destroyBody();
                coins.removeValue(c, true);
            }

        }

        for (Life l : lifes) {
            if (l.toDestroy) {
                l.destroyBody();
                lifes.removeValue(l, true);
            }

        }

        for (Shield s : shields) {
            if (s.toDestroy) {
                s.destroyBody();
                shields.removeValue(s, true);
            }

        }

        for (Ultrashield u : ultraShields) {
            if (u.toDestroy) {
                u.destroyBody();
                ultraShields.removeValue(u, true);
            }

        }

    }

    /**
     * Llama a la funcion draw de todas las entidades
     * de este nivel para que se pinten
     *
     * @param dt  tiempo entre cada frame que se renderiza
     * @param batch pinta la primera imagen de la textura activa
     */
    public void render(float dt, SpriteBatch batch) {

        for (DynamicElement e : enemies) {
            e.draw(dt, batch);
        }

        for (MovePlatform p : platforms) {
            p.draw(dt, batch);
        }

        for (Coin c : coins) {
            c.draw(dt, batch);
        }
        for (Life l : lifes) {
            l.draw(dt, batch);
        }
        for (Shield s : shields) {
            s.draw(dt, batch);
        }
        for (Ultrashield u : ultraShields) {
            u.draw(dt, batch);
        }
        if (player != null) {
            player.draw(dt, batch);
        }

        if (winScreen != null) {
            winScreen.draw(dt, batch);
        }

        if (finalLevel != null) {
            finalLevel.draw(dt, batch);
        }

        if (nextLevel != null) {
            nextLevel.draw(dt, batch);
        }

        if (powerUp != null) {
            powerUp.draw(dt, batch);
        }
    }

    /**
     * Para que pinte las texturas
     * @param dt tiempo entre cada frame que se renderiza
     * @param batch pinta la primera imagen de la textura activa
     */
    public void postrender(float dt, SpriteBatch batch) {

        for (DynamicElement e : enemies) {
            e.postDraw(dt, batch);
        }

        for (MovePlatform p : platforms) {
            p.postDraw(dt, batch);
        }

        if (player != null) {
            player.postDraw(dt, batch);
        }
    }

    /**
     * Cuando se finalice el juego se muestre la pantalla menu
     */
    public void endGame() {
        gameScreen.game.setScreen(new MenuScreen(gameScreen.game));
        gameScreen.dispose();
    }

    /**
     * Para los sonidos del enemigo
     */
    public void dispose() {
        for (DynamicElement dynamicElement : enemies) {
            if (dynamicElement instanceof Ghost) {
                Ghost.soundGhost.dispose();
            }
        }
    }
}
