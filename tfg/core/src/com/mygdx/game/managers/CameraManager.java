package com.mygdx.game.managers;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.mygdx.game.characters.DynamicElement;
import com.mygdx.game.util.Constants;

/**
 * Clase que sigue al jugador para mostrar la zona en  la que se encuentra
 */
public class CameraManager {

	private LevelManager level;
	public OrthographicCamera camera;
	public Viewport viewPort;

	/**
	 * Se muestra el ancho y alto de la pantalla
	 * @param level se pasa el nivel en el que esta
	 */
	public CameraManager(LevelManager level) {
		this.level = level;
		this.camera = new OrthographicCamera();
		this.viewPort = new FitViewport(Constants.SCREEN_WIDTH/Constants.PIXELES_MM, Constants.SCREEN_HEIGHT/Constants.PIXELES_MM, camera);
		
	}

	/**
	 * Metodo para que la camara se mueva verticalmente,
	 * ya que el jugador tiene que subir hacia arriba
	 * @param dt tiempo entre cada frame que se renderiza
	 * @param subject para ir movimiendo la camara
	 */
	public void update(float dt, DynamicElement subject){
		//camera.position.x += (subject.body.getPosition().x + dt - camera.position.x) * 2.1f * dt; // para que la camara se mueva horizontalmente
		camera.position.y += (subject.body.getPosition().y + dt - camera.position.y) * 3.1f * dt; // para que la camara se mueva verticalmente

	}
	
	
}
