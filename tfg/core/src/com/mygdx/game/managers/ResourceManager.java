package com.mygdx.game.managers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Json;
import com.mygdx.game.servidor.dto.ScoreDTO;
import com.mygdx.game.util.ConstantsResources;

/**
 * Clase donde se cargan todas las texturas, sonidos...
 */
public class ResourceManager {
	
	public static AssetManager assets = new AssetManager();
	public static String userName;
	public static int lifes = 1;
	public static int coins = 0;
	public static Array<ScoreDTO> scores = new Array<>();

	/**
	 * Cargar la puntuacion del fichero
	 */
	public static void loadScore(){
		try {
			FileHandle file = Gdx.files.local("scores.json");
			String json = file.readString();
			Json j = new Json(); //Clase que ayuda a convertir de json y a json un objeto
			scores = j.fromJson(Array.class, json);
		}catch (Exception e){
			System.out.println(e.getMessage());
		}
	}

	public static void saveScore(){
		try {
			FileHandle file = Gdx.files.local("scores.json");
			Json j = new Json(); //Clase que ayuda a convertir de json y a json un objeto
			j.toJson(scores, Array.class, file);
		}catch (Exception e){
			System.out.println(e.getMessage());
		}
	}

	/**
	 * Se cargan las texturas y sonidos
	 */
	public static void loadAllResources() {

		assets.load(ConstantsResources.MENU_SKIN, Skin.class);

		assets.load(ConstantsResources.PLAYER, TextureAtlas.class);

		for(String s : ConstantsResources.ENEMY){
			assets.load(s, TextureAtlas.class);
		}

		assets.load(ConstantsResources.COIN, TextureAtlas.class );
		assets.load(ConstantsResources.LIFE, TextureAtlas.class );
		assets.load(ConstantsResources.SHIELD, TextureAtlas.class );
		assets.load(ConstantsResources.SHIELD_TEXTURE, Texture.class );
		assets.load(ConstantsResources.ULTRASHIELD, TextureAtlas.class );
		assets.load(ConstantsResources.BULLET, TextureAtlas.class );
		assets.load(ConstantsResources.ULTRASHIELD_TEXTURE, Texture.class );
		assets.load(ConstantsResources.DOOR_NEXTLEVEL, Texture.class );
		assets.load(ConstantsResources.STAR, Texture.class );
		assets.load(ConstantsResources.POWER_UP, Texture.class );
        assets.load(ConstantsResources.TORRE, Texture.class );

		for(String s : ConstantsResources.MOVE_PLATFORM){
			assets.load(s, TextureAtlas.class);
		}

		assets.load(ConstantsResources.SOUND_COIN, Sound.class);
		assets.load(ConstantsResources.SOUND_SHOOT, Sound.class);
		assets.load(ConstantsResources.SOUND_GHOST, Sound.class);
		assets.load(ConstantsResources.SOUND_LIFE, Sound.class);
		assets.load(ConstantsResources.SOUND_SHIELD, Sound.class);
		assets.load(ConstantsResources.SOUND_WIN, Sound.class);
		assets.load(ConstantsResources.SOUND_DOOR, Sound.class);
	}


	/**
	 * Se cargan los estilos
	 * @param path se pasa la ruta de la imagen que queremos cargar
	 * @return assets.get(path,Skin.class) se devuelve la ruta y el estilo
	 */
	public static Skin getSkin(String path) {
		return assets.get(path,Skin.class);
	}

	/**
	 * Para actualizar
	 * @return assets.update()  carga la imagen
	 */
	public static boolean update() {
		return assets.update();
	}

	/**
	 * Para poder emplear las texturas atlas en otras clases
	 * @param path pasa la ruta de la textura atlas
	 * @return assets.get(path, TextureAtlas.class) devuelve la textura atlas
	 */
	public static TextureAtlas getAtlas(String path) {
		return assets.get(path, TextureAtlas.class);

	}

	/**
	 * Para poder emplear los sonidos en otras clases
	 * @param path pasa la ruta del sonido
	 * @return assets.get(path, Sound.class) devuelve el sonido
	 */
	public static Sound getSound(String path) {
		return assets.get(path, Sound.class);
	}
	
	public static void dispose() {
		assets.dispose();
	}

	/**
	 * Para poder emplear las texturas en otras clases
	 * @param path ruta de la textura
	 * @return assets.get(path, Texture.class) delvuelve la textura
	 */
    public static Texture getTexture(String path) {
		return assets.get(path, Texture.class);
    }
}
