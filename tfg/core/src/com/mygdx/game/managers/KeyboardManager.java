package com.mygdx.game.managers;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.utils.async.AsyncExecutor;
import com.badlogic.gdx.utils.async.AsyncResult;
import com.badlogic.gdx.utils.async.AsyncTask;
import com.mygdx.game.screens.GameScreen;
import com.mygdx.game.screens.MenuScreen;
import com.mygdx.game.screens.PlayerNameScreen;
import com.mygdx.game.servidor.Client;
import com.mygdx.game.util.GameStates;

import java.util.concurrent.Executor;

/**
 * Teclas con las que el jugador podra jugar
 */
public class KeyboardManager implements InputProcessor{

	GameScreen gameScreen;


	/**
	 * Constructor donde se inicializa la variable
	 * @param gameScreen GameScreen
	 */
	public KeyboardManager(GameScreen gameScreen) {
		this.gameScreen = gameScreen;
	}

	/**
	 * Lo que ocurre cuando se presionan las teclas
	 * @param keycode se pasan las teclas que van a tener funcionalidad
	 * @return true
	 */
	@Override
	public boolean keyDown(int keycode) {
		  switch (keycode) {
		  case Input.Keys.RIGHT:
	             gameScreen.levelManager.player.left = true;
	             break;
	         case Input.Keys.LEFT:
	             gameScreen.levelManager.player.right = true;
	             break;
	         case Input.Keys.DOWN:
	             gameScreen.levelManager.player.down = true;
	             break;
	         case Input.Keys.UP:
	             gameScreen.levelManager.player.up = true;
	             break;
	         case Input.Keys.P:
	         	if(gameScreen.state == GameStates.PAUSE){
					gameScreen.resume();
				}else {
					gameScreen.pause();
				}
	        	 break;
	         	case Input.Keys.A:
					gameScreen.levelManager.player.bullet_left = true;
					gameScreen.levelManager.player.shoot = true;
					break;
			  case Input.Keys.D:
				  gameScreen.levelManager.player.bullet_right = true;
				  gameScreen.levelManager.player.shoot = true;
				  break;
			  case Input.Keys.W:
				  gameScreen.levelManager.player.bullet_up = true;
				  gameScreen.levelManager.player.shoot = true;
				  break;

          case Input.Keys.ESCAPE:
        	  gameScreen.levelManager.endGame();
              break;
              
		  }
		return true;
		
	}

	/**
	 * Lo que ocurre cuando las teclas dejan de estar presionadas
	 * @param keycode se pasan las teclas que van a dejar
	 *                   de tener funcionalidad cuando no se pulsen
	 * @return true
	 */
	@Override
	public boolean keyUp(int keycode) {
		 switch (keycode) {
         case Input.Keys.RIGHT:
             gameScreen.levelManager.player.left = false;
             break;
         case Input.Keys.LEFT:
             gameScreen.levelManager.player.right = false;
             break;
         case Input.Keys.DOWN:
             gameScreen.levelManager.player.down = false;
             break;
         case Input.Keys.UP:
             gameScreen.levelManager.player.up = false;
             break;
         case Input.Keys.ENTER:
			 AsyncExecutor executor = new AsyncExecutor(10);
			 AsyncResult<Void> task = executor.submit(new AsyncTask<Void>() {
				 @Override
				 public Void call() throws Exception {
					 return null;
				 }
			 });

       		break;
			 case Input.Keys.A:
				 gameScreen.levelManager.player.bullet_left = false;
				 gameScreen.levelManager.player.shoot = false;
				 break;
			 case Input.Keys.D:
				 gameScreen.levelManager.player.bullet_right = false;
				 gameScreen.levelManager.player.shoot = false;
				 break;
			 case Input.Keys.W:
				 gameScreen.levelManager.player.bullet_up = false;
				 gameScreen.levelManager.player.shoot = false;
				 break;

         case Input.Keys.ESCAPE:
       	  gameScreen.levelManager.endGame();
             break;
             
		  }
		return true;
		
	}

	@Override
	public boolean keyTyped(char character) {

		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {

		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {

		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {

		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {

		return false;
	}

	@Override
	public boolean scrolled(int amount) {

		return false;
	}

}
