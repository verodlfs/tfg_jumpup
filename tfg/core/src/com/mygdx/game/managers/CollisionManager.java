package com.mygdx.game.managers;

import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.mygdx.game.characters.DynamicElement;

/**
 * Clase para crear las colisiones de los objetos
 */
public class CollisionManager implements ContactListener{

	/**
	 * Con que colisiona al principio
	 * @param contact objetos con los que va a colisionar
	 */
	@Override
	public void beginContact(Contact contact) {
		if(contact.getFixtureA().getUserData() instanceof DynamicElement) {
			((com.mygdx.game.characters.Contact)contact.getFixtureA().getUserData()).onContact(contact);
		}
		
		if(contact.getFixtureB().getUserData() instanceof DynamicElement) {
			((com.mygdx.game.characters.Contact)contact.getFixtureB().getUserData()).onContact(contact);
		}
		
	}

	/**
	 * Con que colisiona al final
	 * @param contact objetos con los que va a colisionar al final
	 */
	@Override
	public void endContact(Contact contact) {
		
		if(contact.getFixtureA().getUserData() instanceof DynamicElement) {
			((com.mygdx.game.characters.Contact)contact.getFixtureA().getUserData()).endContact(contact);
		}
		
		if(contact.getFixtureB().getUserData() instanceof DynamicElement) {
			((com.mygdx.game.characters.Contact)contact.getFixtureB().getUserData()).endContact(contact);
		}
		
	}


	@Override
	public void preSolve(Contact contact, Manifold oldManifold) {

		
	}


	@Override
	public void postSolve(Contact contact, ContactImpulse impulse) {

		
	}

}
