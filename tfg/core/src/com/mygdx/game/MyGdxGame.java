package com.mygdx.game;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.mygdx.game.screens.LoadScreen;

/**
 * Clase que muestra la primera
 * pantalla al iniciar el juego
 */
public class MyGdxGame extends Game {
	
	public SpriteBatch batch;
	public BitmapFont font;
	public boolean isOnline = false;
	public int level=1;

	/**
	 * Se crea la primera pantalla
	 */
	@Override
	public void create () {

		batch = new SpriteBatch();
		font = new BitmapFont();
		setScreen(new LoadScreen(this));
	}

	/**
	 * Se renderiza to do
	 */
	@Override
	public void render () {
		super.render();
	}

	/**
	 * Se muestra la fuente
	 */
	@Override
	public void dispose () {
		batch.dispose();
		font.dispose();
	}
}
