package com.mygdx.game.characters;

public abstract class Contact {

	/**
	 * Indica con quien colisiona, y se pasa al objeto que lo implementa: Dynamic element : player
	 * @param contact indica con que objeto colisiona
	 */
	public abstract void onContact(com.badlogic.gdx.physics.box2d.Contact contact);
	public abstract void endContact(com.badlogic.gdx.physics.box2d.Contact contact);
	
}
