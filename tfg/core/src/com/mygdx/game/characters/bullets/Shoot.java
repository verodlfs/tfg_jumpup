package com.mygdx.game.characters.bullets;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.World;
import com.mygdx.game.characters.DynamicElement;
import com.mygdx.game.managers.ResourceManager;
import com.mygdx.game.util.*;

/**
 * Clase del disparo del jugador
 */
public class Shoot extends DynamicElement {

    public boolean activated;
    public float velocity = 8f;
    private Directions direction;
    private static Sound soundShoot = ResourceManager.getSound(ConstantsResources.SOUND_SHOOT);
    // Animaciones
    private Animation<TextureRegion> bulletTexture;
    public int spriteSizeTarget = 32;

    /**
     * Constructor donde se inicializan las variables
     * @param map se pasa el mapa
     * @param rect se pasa la colision
     * @param world se pasa el mundo
     * @param direction indica la direccion a la que se dirigirá la bala
     * @param bulletVelocity velocidad a la que se dispara la bala
     */
    public Shoot(TiledMap map, Rectangle rect, World world, Directions direction, float bulletVelocity) {
        super(map, rect, world);

        soundShoot.play(0.3f);

        velocity = bulletVelocity;

        activated = false;
        this.direction = direction;
        fixtureDef.filter.categoryBits = Constants.BULLET;
        fixtureDef.filter.maskBits = Constants.ENEMY | Constants.PLATFORM;// + Constants.enemy + ... ; todas las cosas con las que va a colisionar el personaje
        bulletTexture = new Animation<TextureRegion>(1f, ResourceManager.getAtlas(ConstantsResources.BULLET).findRegions("walk"));

        float bodySize = spriteSizeTarget / Constants.PIXELES_MM;

        createBody(bodySize);
        body.setGravityScale(0);

    }


    /**
     * Se pinta la textura para mostrarla
     * @param dt tiempo entre cada frame que se renderiza
     * @param batch pinta la primera imagen de la textura activa
     */
    @Override
    public void draw(float dt, Batch batch) {

        TextureRegion tr = TextureUtil.getFrame(dt, bulletTexture);
        batch.draw(
                tr,//textura
                body.getPosition().x + Constants.PLAYER_OFFSET_SPRITE_X - (((tr.getRegionHeight()) / Constants.PIXELES_MM) * Constants.COIN_SCALE / 2 ),//posicion x
                body.getPosition().y + Constants.PLAYER_OFFSET_SPRITE_Y - (((tr.getRegionHeight()) / Constants.PIXELES_MM) * Constants.COIN_SCALE / 2 ), //posicion y
                ((tr.getRegionWidth()*0.5f) / Constants.PIXELES_MM) * Constants.COIN_SCALE, //ancho
                ((tr.getRegionHeight()*0.5f) / Constants.PIXELES_MM) * Constants.COIN_SCALE//alto

        );

    }

    @Override
    public void postDraw(float dt, Batch batch) {

    }

    /**
     * Se establecen las direcciones del jugador
     * @param dt tiempo entre cada frame que se renderiza
     */
    @Override
    public void update(float dt) {
        if (direction == Directions.LEFT) { // IZQUIERDA
            body.setLinearVelocity(new Vector2(-velocity, 0));
        } else if (direction == Directions.UP) {//ARIBA
            body.setLinearVelocity(new Vector2(0, velocity));
        }else if (direction == Directions.DOWN){
            body.setLinearVelocity(new Vector2(0, -velocity));
        }else{
            body.setLinearVelocity(new Vector2(velocity, 0));//dcha por defecto
        }
    }

    /**
     * Se indica que ocurrirá cuando la bala colisione con determinados elementos
     * @param contact enemy, platform
     */
    @Override
    public void onContact(Contact contact) {
        if (contact.getFixtureA().getFilterData().categoryBits == Constants.ENEMY) {
          //  soundShoot.play(0.3f);
            toDestroy = true;
            ((DynamicElement)contact.getFixtureA().getUserData()).toDestroy = true;
        }else if(contact.getFixtureB().getFilterData().categoryBits==Constants.ENEMY){
           // soundShoot.play(0.3f);
            //destruye el enemigo
            toDestroy=true;
            ((DynamicElement)contact.getFixtureB().getUserData()).toDestroy = true;
        }

        if (contact.getFixtureA().getFilterData().categoryBits == Constants.PLATFORM) {
            toDestroy = true;
        }else if(contact.getFixtureB().getFilterData().categoryBits==Constants.PLATFORM){
            toDestroy=true;
        }

    }

    @Override
    public void endContact(Contact contact) {

    }
}
