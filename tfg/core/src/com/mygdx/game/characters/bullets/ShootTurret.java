package com.mygdx.game.characters.bullets;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.World;
import com.mygdx.game.characters.DynamicElement;
import com.mygdx.game.managers.ResourceManager;
import com.mygdx.game.util.Constants;
import com.mygdx.game.util.ConstantsResources;
import com.mygdx.game.util.Directions;
import com.mygdx.game.util.TextureUtil;

public class ShootTurret extends DynamicElement {

    public boolean activated;
    public float velocity = 8f;
    private Directions direction;
    private static Sound soundShoot = ResourceManager.getSound(ConstantsResources.SOUND_SHOOT);
    // Animaciones
    private Animation<TextureRegion> bulletTexture;
    public int spriteSizeTarget = 32;


    public ShootTurret(TiledMap map, Rectangle rect, World world, Directions direction, float bulletVelocity) {
        super(map, rect, world);

        velocity = bulletVelocity;
      //  soundShoot.play(0.3f);
        activated = false;
        this.direction = direction;
        fixtureDef.filter.categoryBits = Constants.ENEMY;
        fixtureDef.filter.maskBits =  Constants.PLATFORM | Constants.PLAYER;// + Constants.enemy + ... ; todas las cosas con las que va a colisionar el personaje
        bulletTexture = new Animation<TextureRegion>(1f, ResourceManager.getAtlas(ConstantsResources.BULLET).findRegions("walk"));

        float bodySize = spriteSizeTarget / Constants.PIXELES_MM;

        createBody(bodySize);
        body.setGravityScale(0);

    }


    @Override
    public void draw(float dt, Batch batch) {
        TextureRegion tr = TextureUtil.getFrame(dt, bulletTexture);
        batch.draw(
                tr,//textura
                body.getPosition().x + Constants.PLAYER_OFFSET_SPRITE_X - (((tr.getRegionHeight()) / Constants.PIXELES_MM) * Constants.COIN_SCALE / 2 ),//posicion x
                body.getPosition().y + Constants.PLAYER_OFFSET_SPRITE_Y - (((tr.getRegionHeight()) / Constants.PIXELES_MM) * Constants.COIN_SCALE / 2 ), //posicion y
                ((tr.getRegionWidth()*0.5f) / Constants.PIXELES_MM) * Constants.COIN_SCALE, //ancho
                ((tr.getRegionHeight()*0.5f) / Constants.PIXELES_MM) * Constants.COIN_SCALE//alto

        );

    }

    @Override
    public void postDraw(float dt, Batch batch) {

    }

    @Override
    public void update(float dt) {
        if (direction == Directions.LEFT) { // IZQUIERDA
            body.setLinearVelocity(new Vector2(-velocity, 0));
        } else if (direction == Directions.UP) {//ARIBA
            body.setLinearVelocity(new Vector2(0, velocity));
        }else if (direction == Directions.DOWN){
            body.setLinearVelocity(new Vector2(0, -velocity));
        }else{
            body.setLinearVelocity(new Vector2(velocity, 0));//dcha por defecto
        }



    }

    @Override
    public void onContact(Contact contact) {
        if (contact.getFixtureA().getFilterData().categoryBits == Constants.ENEMY) {
         //   soundShoot.play(0.3f);
            toDestroy = true;
            ((DynamicElement)contact.getFixtureA().getUserData()).toDestroy = true;
        }else if(contact.getFixtureB().getFilterData().categoryBits==Constants.ENEMY){
        //    soundShoot.play(0.3f);
            toDestroy=true;
            ((DynamicElement)contact.getFixtureB().getUserData()).toDestroy = true;//destruye el enemigo
        }

        if (contact.getFixtureA().getFilterData().categoryBits == Constants.PLATFORM) {
            toDestroy = true;
        }else if(contact.getFixtureB().getFilterData().categoryBits==Constants.PLATFORM){
            toDestroy=true;
        }

    }

    @Override
    public void endContact(Contact contact) {

    }
}
