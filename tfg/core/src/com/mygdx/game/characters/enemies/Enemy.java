package com.mygdx.game.characters.enemies;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.EdgeShape;
import com.badlogic.gdx.physics.box2d.World;
import com.mygdx.game.characters.DynamicElement;
import com.mygdx.game.managers.ResourceManager;
import com.mygdx.game.screens.Hud;
import com.mygdx.game.util.Animations;
import com.mygdx.game.util.Constants;
import com.mygdx.game.util.ConstantsResources;
import com.mygdx.game.util.TextureUtil;

/**
 * Clase de los enemigos que andan
 */

public class Enemy extends DynamicElement {
	
	private Animation<TextureRegion> animationWalk;
	private Animation<TextureRegion> animationWalkLeft;

	public float animationProgress;
	public Animations lastAnimation;
	public Animations actualAnimation = Animations.WALK_RIGHT;
	private Animation<TextureRegion> actualAnimationTexture;

	public boolean lastDirection = true;
	public boolean directionChange = false;
	public float enemyScale;


	public int spriteSizeTarget=32;
	public float spriteSizeTargetValue=1f;

	/**
	 * Constructor donde se inicializan las variables
	 * @param map se pasa el mapa
	 * @param rect se pasa la colision
	 * @param world se pasa el mundo
	 * @param skinId se pasa el numero de la textura
	 * @param enemyScale se da un tamaño a la imagen
	 */
	public Enemy(TiledMap map, Rectangle rect, World world, int skinId, float enemyScale) {
		super(map, rect, world);
		this.enemyScale = enemyScale;

		fixtureDef.filter.categoryBits = Constants.ENEMY;
		fixtureDef.filter.maskBits = Constants.PLATFORM | Constants.PLAYER | Constants.BULLET;// + Constants.enemy + ... ; todas las cosas con las que va a colisionar el personaje

		animationWalk = new Animation<TextureRegion>(0.1f, ResourceManager.getAtlas(ConstantsResources.ENEMY[skinId]).findRegions("walk"));
		animationWalkLeft = new Animation<TextureRegion>(0.1f, ResourceManager.getAtlas(ConstantsResources.ENEMY[skinId]).findRegions("left_walk"));

		float bodySize = ((spriteSizeTarget*Constants.ENEMY_BODY_SCALE)/Constants.PIXELES_MM)*enemyScale;

		spriteSizeTargetValue = calculateSpriteSizeTarget(TextureUtil.getFrame(0,animationWalk).getRegionHeight());

		createBody(bodySize);

		EdgeShape bodyShape2 = new EdgeShape();
		bodyShape2.set(new Vector2(bodySize*0.5f,-bodySize*0.75f),new Vector2(bodySize*0.5f, 0));
		fixtureDef.filter.categoryBits = Constants.SENSOR_LEFT_ENEMY;
		fixtureDef.shape = bodyShape2;
		fixtureDef.filter.maskBits = Constants.PLATFORM;
		fixtureDef.isSensor = true;
		body.createFixture(fixtureDef).setUserData(this);
		
		EdgeShape bodyShape3 = new EdgeShape();
		bodyShape3.set(new Vector2(-bodySize*0.5f,-bodySize*0.75f),new Vector2(-bodySize*0.5f, 0));
		fixtureDef.filter.categoryBits = Constants.SENSOR_RIGHT_ENEMY;
		fixtureDef.shape = bodyShape3;
		fixtureDef.filter.maskBits = Constants.PLATFORM;
		fixtureDef.isSensor = true;
		body.createFixture(fixtureDef).setUserData(this);
		
	}


	/**
	 * Se obtiene la imagen que corresponda de la animacion.
	 * @param dt tiempo entre cada frame que se renderiza
	 * @param batch pinta la primera imagen de la textura activa
	 */
	@Override
	public void draw(float dt, Batch batch) {


		if(actualAnimation == lastAnimation){
			animationProgress = animationProgress + Gdx.graphics.getDeltaTime();
		}else{
			animationProgress = 0;
		}

		lastAnimation = actualAnimation;

		TextureRegion tr = actualAnimationTexture.getKeyFrame(animationProgress, true);
		
		batch.draw(
				tr,//textura 
				body.getPosition().x - ((spriteSizeTarget/2)/Constants.PIXELES_MM)*enemyScale,//posicion x
				body.getPosition().y - ((spriteSizeTarget/2)/Constants.PIXELES_MM)*enemyScale-12/Constants.PIXELES_MM, //posicion y
				(spriteSizeTarget/Constants.PIXELES_MM)*enemyScale, //ancho
				(spriteSizeTarget/Constants.PIXELES_MM) *enemyScale );//alto

	}

	/**
	 * Se calcula la imagen que toca en cada momento
	 */
	private void calculateActualAnimation(){

		if(body.getLinearVelocity().y == 0) {

			if(body.getLinearVelocity().x > 0){
				actualAnimation = Animations.WALK_RIGHT;
				actualAnimationTexture = animationWalk;
			}else if(body.getLinearVelocity().x < 0){
				actualAnimation = Animations.WALK_LEFT;
				actualAnimationTexture = animationWalkLeft;
			}
		}
	}

	/**
	 * Calcula el tamaño de la imagen que se va a pintar
	 * @param height altura de la imagen
	 * @return spriteSizeTarget/height
	 */
	private float calculateSpriteSizeTarget(float height) {
		return spriteSizeTarget/height;
	}

	@Override
	public void postDraw(float dt, Batch batch) {


	}

	/**
	 * Se indica la fuerza del movimiento de los enemigos
	 * @param dt tiempo entre cada frame que se renderiza
	 */
	@Override
	public void update(float dt) {
		if(lastDirection && body.getLinearVelocity().x < 2) {
			body.applyLinearImpulse(new Vector2(0.20f, 0), body.getWorldCenter(), true);

		}else if(body.getLinearVelocity().x > -2){

			body.applyLinearImpulse(new Vector2(-0.20f, 0), body.getWorldCenter(), true);
		}
		calculateActualAnimation();
		
	}

	@Override
	public void onContact(Contact contact) {
		
	}


	/**
	 * Metodo de fin de contacto con lo que se indique, por lo que cuando el sensor toque uno de los extremos
	 * gire hacia el otro lado.
	 * @param contact objetos con los que va a colisionar
	 */
	@Override
	public void endContact(Contact contact) {
		
		boolean left = false;
		boolean right = false;
		
		
			//LEFT 
		
			if(contact.getFixtureA().getFilterData().categoryBits == Constants.SENSOR_LEFT_ENEMY) {
				left = true;
			}
			if(contact.getFixtureB().getFilterData().categoryBits == Constants.SENSOR_LEFT_ENEMY) {
				left = true;
			}
			
			//RIGHT
			
			if(contact.getFixtureA().getFilterData().categoryBits == Constants.SENSOR_RIGHT_ENEMY) {
				right = true;
			}
			if(contact.getFixtureB().getFilterData().categoryBits == Constants.SENSOR_RIGHT_ENEMY) {
				right = true;
			}
			
			if(right && left) {
				return;
			}
			
			if(!right && !left) {
				return;
			}
			
			if(right) {
				lastDirection = true;
				directionChange = true;
				return;
			}
			
			if(left) {
				lastDirection = false;
				directionChange = true;
				return;
			}
		
	}

}
