package com.mygdx.game.characters.enemies;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.EdgeShape;
import com.badlogic.gdx.physics.box2d.World;
import com.mygdx.game.characters.DynamicElement;
import com.mygdx.game.managers.ResourceManager;
import com.mygdx.game.screens.GameScreen;
import com.mygdx.game.util.*;

/**
 * Enemigo que persigue al jugador volando
 */
public class Ghost extends DynamicElement {

	public static Sound soundGhost = ResourceManager.getSound(ConstantsResources.SOUND_GHOST);

	public final GameScreen gameScreen;
	public int spriteSizeTarget=32;

	private Animation<TextureRegion> animationWalk;
	private Animation<TextureRegion> animationWalkLeft;

	public float animationProgress;
	public Animations lastAnimation;
	public Animations actualAnimation = Animations.WALK_RIGHT;
	private Animation<TextureRegion> actualAnimationTexture;

	/**
	 * Se inicializan las variables y se crea la colision
	 * @param map se pasa el mapa
	 * @param rect se pasa la colision
	 * @param world se pasa el mundo
	 * @param skinId se pasa el id de la imagen correspondiente
	 * @param gameScreen se pasa la pantalla game para poder
	 * 					acceder a sus variables y metodos
	 */
	public Ghost(TiledMap map, Rectangle rect, World world,int skinId, final GameScreen gameScreen) {
		super(map, rect, world);
		this.gameScreen = gameScreen;
		fixtureDef.filter.categoryBits = Constants.ENEMY;
		fixtureDef.filter.maskBits = Constants.PLAYER | Constants.BULLET;// + Constants.enemy + ... ; todas las cosas con las que va a colisionar el personaje

		// Esta linea correspondera el nombre con cada animacion
		animationWalk = new Animation<TextureRegion>(1/4f, ResourceManager.getAtlas(ConstantsResources.ENEMY[skinId]).findRegions("walk"));
		actualAnimationTexture = animationWalk;
		// Se utiliza un frame de la animacion del personaje para obtener sus dimensiones
		float bodySize = ((spriteSizeTarget*Constants.ENEMY_BODY_SCALE)/Constants.PIXELES_MM);

		createBody(bodySize);

		body.setGravityScale(0);
		

		
	}

	/**
	 * Se obtiene la imagen que corresonda de la animacion y se pinta
	 * @param dt tiempo entre cada frame que se renderiza
	 * @param batch pinta la primera imagen de la textura activa
	 */
	@Override
	public void draw(float dt, Batch batch) {

		if(actualAnimation == lastAnimation){
			animationProgress = animationProgress + Gdx.graphics.getDeltaTime();
		}else{
			animationProgress = 0;
		}

		lastAnimation = actualAnimation;
		//Se obtiene la imagen que corresonda de la animacion

		TextureRegion tr = actualAnimationTexture.getKeyFrame(animationProgress, true);


		batch.draw(
				tr,//textura 
				body.getPosition().x + Constants.ENEMY_OFFSET_SPRITE_X  - (((tr.getRegionHeight()*0.5f)/Constants.PIXELES_MM) * Constants.ENEMY_BASE_SALE/2),//posicion x
				body.getPosition().y + Constants.ENEMY_OFFSET_SPRITE_Y - (((tr.getRegionHeight()*0.5f)/Constants.PIXELES_MM) *Constants.ENEMY_BASE_SALE/2), //posicion y
				((tr.getRegionWidth()*0.5f)/Constants.PIXELES_MM)*Constants.ENEMY_BASE_SALE, //ancho
				((tr.getRegionHeight()*0.5f)/Constants.PIXELES_MM) *Constants.ENEMY_BASE_SALE);//alto


		
	}

	@Override
	public void postDraw(float dt, Batch batch) {

		
	}

	/**
	 * Se establece un rango de vision en el que el enemigo se va a mover hacia el jugador
	 * @param dt tiempo entre cada frame que se renderiza
	 */
	@Override
	public void update(float dt) {

		if(CalculationUtil.calculateRange(gameScreen.levelManager.player.body, body, 500/Constants.PIXELES_MM )){
			//el enemigo se mueve hacia el jugador, esta en rango de vision
			if((gameScreen.levelManager.player.body.getPosition().x < body.getPosition().x) && body.getLinearVelocity().x > -2){
				soundGhost.play(0.1f);
				body.applyLinearImpulse(new Vector2(-0.3f, 0), body.getWorldCenter(), true);

			}
			else if(gameScreen.levelManager.player.body.getPosition().x > body.getPosition().x && body.getLinearVelocity().x < 2){

				body.applyLinearImpulse(new Vector2(0.1f, 0), body.getWorldCenter(), true);

			}
			if(gameScreen.levelManager.player.body.getPosition().y < body.getPosition().y && body.getLinearVelocity().y > -2){

				body.applyLinearImpulse(new Vector2(0f, -0.3f), body.getWorldCenter(), true);

			}
			else if(gameScreen.levelManager.player.body.getPosition().y > body.getPosition().y && body.getLinearVelocity().y < 2){

				body.applyLinearImpulse(new Vector2(0, 0.1f), body.getWorldCenter(), true); //Para que mantenga el recorrido

			}
		}
		calculateActualAnimation();

		
	}

	/**
	 * Se calcula la animacion que toca en cada caso
	 */
	private void calculateActualAnimation(){

		if(body.getLinearVelocity().y == 0) {

			if(body.getLinearVelocity().x > 0){
				actualAnimation = Animations.WALK_RIGHT;
				actualAnimationTexture = animationWalk;
			}else if(body.getLinearVelocity().x < 0){
				actualAnimation = Animations.WALK_LEFT;
				actualAnimationTexture = animationWalkLeft;
			}
		}
	}

	@Override
	public void onContact(Contact contact) {
		
	}



	@Override
	public void endContact(Contact contact) {

		
	}

}
