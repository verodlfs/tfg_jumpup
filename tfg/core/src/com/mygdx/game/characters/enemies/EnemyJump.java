package com.mygdx.game.characters.enemies;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.EdgeShape;
import com.badlogic.gdx.physics.box2d.World;
import com.mygdx.game.characters.DynamicElement;
import com.mygdx.game.managers.ResourceManager;
import com.mygdx.game.util.Animations;
import com.mygdx.game.util.Constants;
import com.mygdx.game.util.ConstantsResources;
import com.mygdx.game.util.TextureUtil;

/**
 * Tipo de enemigos que saltan
 */
public class EnemyJump extends DynamicElement {

	private Animation<TextureRegion> animationWalk;
	private Animation<TextureRegion> animationWalkLeft;

	public float enemyScale;

	public float animationProgress;
	public Animations lastAnimation;
	public Animations actualAnimation = Animations.WALK_RIGHT;
	private Animation<TextureRegion> actualAnimationTextureJump;

	public int spriteSizeTarget=32;
	public float spriteSizeTargetValue=1f;

	/**
	 * Se inicializan las variables y se establece la colision
	 * @param map se pasa el mapa
	 * @param rect se pasa la colision
	 * @param world se pasa el mundo
	 * @param skinId se pasa el id de la imagen que se quiere mostrar
	 * @param enemyScale se da un tamaño a la imagen
	 */
	public EnemyJump(TiledMap map, Rectangle rect, World world, int skinId, float enemyScale) {
		super(map, rect, world);
		this.enemyScale = enemyScale;

		fixtureDef.filter.categoryBits = Constants.ENEMY;
		//Se indican todos los objetos con los que va a colisionar el Enemy
		fixtureDef.filter.maskBits = Constants.PLATFORM | Constants.PLAYER | Constants.BULLET;// + Constants.enemy + ... ; todas las cosas con las que va a colisionar el personaje

		animationWalk = new Animation<TextureRegion>(0.1f, ResourceManager.getAtlas(ConstantsResources.ENEMY[skinId]).findRegions("walk"));
		animationWalkLeft = new Animation<TextureRegion>(0.1f, ResourceManager.getAtlas(ConstantsResources.ENEMY[skinId]).findRegions("left_walk"));

		actualAnimationTextureJump = animationWalk;

		float bodySize = ((spriteSizeTarget*Constants.ENEMY_BODY_SCALE)/Constants.PIXELES_MM)*enemyScale;

		spriteSizeTargetValue = calculateSpriteSizeTarget(TextureUtil.getFrame(0,animationWalk).getRegionHeight());

		createBody(bodySize);

		EdgeShape bodyShape2 = new EdgeShape();
		bodyShape2.set(new Vector2(bodySize*0.5f,-bodySize*0.75f),new Vector2(bodySize*0.5f, 0));
		fixtureDef.filter.categoryBits = Constants.SENSOR_LEFT_ENEMY;
		fixtureDef.shape = bodyShape2;
		fixtureDef.filter.maskBits = Constants.PLATFORM;
		fixtureDef.isSensor = true;
		body.createFixture(fixtureDef).setUserData(this);
		
		EdgeShape bodyShape3 = new EdgeShape();
		bodyShape3.set(new Vector2(-bodySize*0.5f,-bodySize*0.75f),new Vector2(-bodySize*0.5f, 0));
		fixtureDef.filter.categoryBits = Constants.SENSOR_RIGHT_ENEMY;
		fixtureDef.shape = bodyShape3;
		fixtureDef.filter.maskBits = Constants.PLATFORM;
		fixtureDef.isSensor = true;
		body.createFixture(fixtureDef).setUserData(this);
		body.getFixtureList().first().setRestitution(1f); // rebota

		
	}


	/**
	 * Se pinta la animacion correspondiente
	 * @param dt tiempo entre cada frame que se renderiza
	 * @param batch pinta la primera imagen de la textura activa
	 */
	@Override
	public void draw(float dt, Batch batch) {

		if(actualAnimation == lastAnimation){
			animationProgress = animationProgress + Gdx.graphics.getDeltaTime();
		}else{
			animationProgress = 0;
		}

		lastAnimation = actualAnimation;
		//Se obtiene la imagen que corresonda de la animacion
		TextureRegion tr = actualAnimationTextureJump.getKeyFrame(animationProgress, true);

		//Para voltear la textura del enemigo
		//tr.flip(true, false);
		
		batch.draw(
				tr,//textura 
				body.getPosition().x - ((spriteSizeTarget/2)/Constants.PIXELES_MM)*enemyScale,//posicion x
				body.getPosition().y - ((spriteSizeTarget/2)/Constants.PIXELES_MM)*enemyScale, //posicion y
				(spriteSizeTarget/Constants.PIXELES_MM)*enemyScale, //ancho
				(spriteSizeTarget/Constants.PIXELES_MM) *enemyScale );//alto

	}

	/**
	 * Se calcula la animacion que toca en cada caso
	 */
	private void calculateActualAnimation(){


		if(body.getLinearVelocity().y == 0) {

			if(body.getLinearVelocity().x > 0){
				actualAnimation = Animations.WALK_RIGHT;
				actualAnimationTextureJump = animationWalk;
			}else if(body.getLinearVelocity().x < 0){
				actualAnimation = Animations.WALK_LEFT;
				actualAnimationTextureJump = animationWalkLeft;
			}
		}
	}

	/**
	 *  Calcula el tamaño de la imagen que se va a pintar
	 * @param height altura de la imagen
	 * @return  spriteSizeTarget/height
	 */
	private float calculateSpriteSizeTarget(float height) {
		return spriteSizeTarget/height;
	}

	@Override
	public void postDraw(float dt, Batch batch) {


	}

	/**
	 * Llama al metodo calcular animacion
	 * @param dt tiempo entre cada frame que se renderiza
	 */
	@Override
	public void update(float dt) {

		calculateActualAnimation();
	}

	@Override
	public void onContact(Contact contact) {
		
	}



	@Override
	public void endContact(Contact contact) {

	}

}
