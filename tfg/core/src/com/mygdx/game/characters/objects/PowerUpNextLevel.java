package com.mygdx.game.characters.objects;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.World;
import com.mygdx.game.MyGdxGame;
import com.mygdx.game.characters.StaticElement;
import com.mygdx.game.managers.ResourceManager;
import com.mygdx.game.screens.GameScreen;
import com.mygdx.game.util.Constants;
import com.mygdx.game.util.ConstantsResources;

public class PowerUpNextLevel extends StaticElement {

    private Texture texture;


    public PowerUpNextLevel(Rectangle rect, GameScreen game, short category) {
        super(rect, game, category);

        texture = ResourceManager.getTexture(ConstantsResources.POWER_UP);
        body = game.world.createBody(bodyDef);
        shape.setAsBox(rect.getWidth()/2/Constants.PIXELES_MM, rect.getHeight()/2/Constants.PIXELES_MM);
        fixtureDef.shape = shape;
        fixtureDef.filter.categoryBits = category;
        body.createFixture(fixtureDef).setUserData(this);
        body.setGravityScale(0);
    }




    @Override
    public void draw(float delta, Batch batch) {
        batch.draw(
                texture,//textura
                body.getPosition().x + Constants.PLAYER_OFFSET_SPRITE_X  - (((texture.getHeight()*0.15f)/Constants.PIXELES_MM) * Constants.SHIELD_SCALE /2),//posicion x
                body.getPosition().y + Constants.PLAYER_OFFSET_SPRITE_Y - (((texture.getWidth()*0.13f)/Constants.PIXELES_MM) *Constants.SHIELD_SCALE /2), //posicion y
                ((texture.getWidth()*0.25f)/Constants.PIXELES_MM)*Constants.SHIELD_SCALE, //ancho
                ((texture.getHeight()*0.25f)/Constants.PIXELES_MM) *Constants.SHIELD_SCALE//alto

        );
    }

    @Override
    public void onContact(Contact contact) {

    }

    @Override
    public void endContact(Contact contact) {

    }
}
