package com.mygdx.game.characters.objects;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.physics.box2d.Contact;
import com.mygdx.game.characters.StaticElement;
import com.mygdx.game.characters.player.Player;
import com.mygdx.game.managers.ResourceManager;
import com.mygdx.game.screens.GameScreen;
import com.mygdx.game.util.Constants;
import com.mygdx.game.util.ConstantsResources;

/**
 * Clase que nos lleva a otro nivel
 */
public class NextLevel extends StaticElement {

    private Texture texture;

    /**
     * Constructor donde se incializan las variables y la colision
     * @param rect se pasa la colision
     * @param game se pasa la pantalla game para poder
     *             acceder a sus variables y metodos
     * @param category es la categoria del elemento
     */
    public NextLevel(Rectangle rect, GameScreen game, short category) {
        super(rect, game, category);

        texture = ResourceManager.getTexture(ConstantsResources.DOOR_NEXTLEVEL);
        body = game.world.createBody(bodyDef);
        shape.setAsBox(rect.getWidth()/2/Constants.PIXELES_MM, rect.getHeight()/2/Constants.PIXELES_MM);
        fixtureDef.shape = shape;
        fixtureDef.filter.categoryBits = category;
        body.createFixture(fixtureDef).setUserData(this);
        body.setGravityScale(0);
    }

    /**
     * Se pinta la textura
     * @param delta tiempo entre cada frame que se renderiza
     * @param batch pinta la primera imagen de la textura activa
     */
    @Override
    public void draw(float delta, Batch batch) {
        batch.draw(
                texture,//textura
                body.getPosition().x + Constants.PLAYER_OFFSET_SPRITE_X  - (((texture.getHeight()*0.15f)/Constants.PIXELES_MM) * Constants.SHIELD_SCALE /2),//posicion x
                body.getPosition().y + Constants.PLAYER_OFFSET_SPRITE_Y - (((texture.getWidth()*0.13f)/Constants.PIXELES_MM) *Constants.SHIELD_SCALE /2), //posicion y
                ((texture.getWidth()*0.25f)/Constants.PIXELES_MM)*Constants.SHIELD_SCALE, //ancho
                ((texture.getHeight()*0.25f)/Constants.PIXELES_MM) *Constants.SHIELD_SCALE//alto

        );
    }

    @Override
    public void onContact(Contact contact) {

    }

    @Override
    public void endContact(Contact contact) {

    }
}
