package com.mygdx.game.characters.objects;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.World;
import com.mygdx.game.characters.DynamicElement;
import com.mygdx.game.managers.ResourceManager;
import com.mygdx.game.util.Constants;
import com.mygdx.game.util.ConstantsResources;
import com.mygdx.game.util.TextureUtil;

/**
 * Elemento "power up" que ayuda al jugador a ser inmune
 * y evitar que los enemigos lo maten.
 */
public class Shield extends DynamicElement {

    private static Sound soundShield = ResourceManager.getSound(ConstantsResources.SOUND_SHIELD);
    private Texture  texture;

    /**
     * Se inicializan las variables, se establece la colision,
     * con que objetos va a poder colisionar y se carga la textura
     * @param map  se carga el mapa
     * @param rect se carga la colision
     * @param world es la categoria del elemento
     */
    public Shield(TiledMap map, Rectangle rect, World world) {
        super(map, rect, world);
        fixtureDef.filter.categoryBits = Constants.SHIELD;
        //todas las cosas con las que va a colisionar
        fixtureDef.filter.maskBits = Constants.PLATFORM |Constants.PLAYER;

        texture = ResourceManager.getTexture(ConstantsResources.SHIELD_TEXTURE);

        createBodyRectangle( 10*Constants.SHIELD_BODY_SCALE/Constants.PIXELES_MM);
        body.getFixtureList().first().setRestitution(1f); //rebota
        body.setGravityScale(0);

    }

    /**
     * Se pinta la textura
     * @param dt tiempo entre cada frame que se renderiza
     * @param batch pinta la primera imagen de la textura activa
     */
    @Override
    public void draw(float dt, Batch batch) {

        batch.draw(
                texture,//textura
                body.getPosition().x + Constants.PLAYER_OFFSET_SPRITE_X  - (((texture.getHeight()*0.3f)/Constants.PIXELES_MM) * Constants.SHIELD_SCALE /2),//posicion x
                body.getPosition().y + Constants.PLAYER_OFFSET_SPRITE_Y - (((texture.getWidth()*0.3f)/Constants.PIXELES_MM) *Constants.SHIELD_SCALE /2), //posicion y
                ((texture.getWidth()*0.25f)/Constants.PIXELES_MM)*Constants.SHIELD_SCALE, //ancho
                ((texture.getHeight()*0.25f)/Constants.PIXELES_MM) *Constants.SHIELD_SCALE//alto

        );
    }

    @Override
    public void postDraw(float dt, Batch batch) {

    }

    @Override
    public void update(float dt) {

    }

    /**
     * Lo que ocurre cuando se colisiona con el escudo es lo siguiente;
     * se destruye y a la vez se emite un sonido
     * @param contact player (objetos con los que va a colisionar)
     */
    @Override
    public void onContact(Contact contact) {
        if(contact.getFixtureA().getFilterData().categoryBits == Constants.PLAYER) {

          soundShield.play(0.3f);
          this.toDestroy = true;

        }

        if(contact.getFixtureB().getFilterData().categoryBits == Constants.PLAYER) {
            soundShield.play(0.3f);
            this.toDestroy = true;
        }

    }

    @Override
    public void endContact(Contact contact) {

    }
}
