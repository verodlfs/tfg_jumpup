package com.mygdx.game.characters.objects;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.World;
import com.mygdx.game.characters.DynamicElement;
import com.mygdx.game.characters.player.Player;
import com.mygdx.game.managers.ResourceManager;
import com.mygdx.game.util.Constants;
import com.mygdx.game.util.ConstantsResources;
import com.mygdx.game.util.TextureUtil;

/**
 * Clase de la moneda
 */
public class Coin extends DynamicElement {

    private static Sound soundCoin = ResourceManager.getSound(ConstantsResources.SOUND_COIN);
    private Animation<TextureRegion> coinTexture;

    /**
     * Se inicializan las variables y se establece la colision
     * @param map   se pasa el mapa
     * @param rect  se pasa la colision
     * @param world se pasa el mundo
     */
    public Coin(TiledMap map, Rectangle rect, World world) {
        super(map, rect, world);
        fixtureDef.filter.categoryBits = Constants.COIN;
        fixtureDef.filter.maskBits = Constants.PLATFORM |Constants.PLAYER; //todas las cosas con las que va a colisionar la coin

        coinTexture = new Animation<TextureRegion>(1/4f, ResourceManager.getAtlas(ConstantsResources.COIN).findRegion("coin"));

        createBodyRectangle( 10*Constants.COIN_BODY_SCALE/Constants.PIXELES_MM /*TextureUtil.getFrame(0,null).getRegionWidth()*Constants.COIN_BODY_SCALE*/);
        body.getFixtureList().first().setRestitution(1f); // rebota
        body.setGravityScale(0);

    }

    /**
     * Se pinta la textura
     * @param dt tiempo entre cada frame que se renderiza
     * @param batch pinta la primera imagen de la textura activa
     */
    @Override
    public void draw(float dt, Batch batch) {
        TextureRegion tr = TextureUtil.getFrame(dt, coinTexture);//obtenemos la imagen que corresonda de la animacion

        batch.draw(
                tr,//textura
                body.getPosition().x + Constants.PLAYER_OFFSET_SPRITE_X  - ((tr.getRegionHeight()/Constants.PIXELES_MM) * Constants.COIN_SCALE/2+0.11f),//posicion x
                body.getPosition().y + Constants.PLAYER_OFFSET_SPRITE_Y - ((tr.getRegionHeight()/Constants.PIXELES_MM) *Constants.COIN_SCALE/2 + 0.1f), //posicion y
                (tr.getRegionWidth()/Constants.PIXELES_MM)*Constants.COIN_SCALE , //ancho
                (tr.getRegionHeight()/Constants.PIXELES_MM) *Constants.COIN_SCALE//alto

        );
    }

    @Override
    public void postDraw(float dt, Batch batch) {

    }

    @Override
    public void update(float dt) {

    }

    /**
     * Se indica con que objetos va a colisionar la moneda.
     * Cuando el jugador colisione con la moneda, se añadirá un
     * punto al marcador y ésta se destruirá.
     * @param contact player (objetos con los que va a colisionar)
     */
    @Override
    public void onContact(Contact contact) {
        if(contact.getFixtureA().getFilterData().categoryBits == Constants.PLAYER) {
          soundCoin.play(0.3f);
            ((Player) contact.getFixtureA().getUserData()).coin++;
            this.toDestroy = true;
        }

        if(contact.getFixtureB().getFilterData().categoryBits == Constants.PLAYER) {
            soundCoin.play(0.3f);
            ((Player) contact.getFixtureB().getUserData()).coin++;
            this.toDestroy = true;
        }

    }

    @Override
    public void endContact(Contact contact) {

    }
}
