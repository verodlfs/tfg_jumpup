package com.mygdx.game.characters.objects;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.physics.box2d.Contact;
import com.mygdx.game.characters.StaticElement;
import com.mygdx.game.screens.GameScreen;

/**
 * Clase que genera las colisiones de las plataformas
 */
public class Platform extends StaticElement {

	public Platform(Rectangle rect, GameScreen game, short category) {
		super(rect, game, category);
	}

	@Override
	public void draw(float delta, Batch batch) {

		
	}

	@Override
	public void onContact(Contact contact) {

		
	}

	@Override
	public void endContact(Contact contact) {

		
	}

}
