package com.mygdx.game.characters.objects;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.World;
import com.mygdx.game.characters.DynamicElement;
import com.mygdx.game.characters.bullets.Shoot;
import com.mygdx.game.characters.bullets.ShootTurret;
import com.mygdx.game.managers.ResourceManager;
import com.mygdx.game.screens.GameScreen;
import com.mygdx.game.util.Constants;
import com.mygdx.game.util.ConstantsResources;
import com.mygdx.game.util.Directions;

public class Torre extends DynamicElement {

    private Texture textureTorre;
    GameScreen gameScreen;
    public float shootVelocity = 65f;
    public float lastShoot = 0;

    public Torre(TiledMap map, Rectangle rect, World world, GameScreen gameScreen) {
        super(map, rect, world);
        this.gameScreen = gameScreen;

        textureTorre = ResourceManager.getTexture(ConstantsResources.TORRE);
        createBodyRectangle(16/Constants.PIXELES_MM,32/Constants.PIXELES_MM);
    }

    @Override
    public void draw(float dt, Batch batch) {
        batch.draw(
                textureTorre,//textura
                body.getPosition().x - 32/Constants.PIXELES_MM,//posicion x
                body.getPosition().y - 32/Constants.PIXELES_MM, //posicion y
                ((textureTorre.getWidth())/Constants.PIXELES_MM), //ancho
                ((textureTorre.getHeight())/Constants.PIXELES_MM) //alto

        );
    }

    @Override
    public void postDraw(float dt, Batch batch) {

    }

    @Override
    public void update(float dt) {


        if (lastShoot <= 0 ) {
                gameScreen.levelManager.enemies.add(
                        new ShootTurret(map, new Rectangle(body.getPosition().x*Constants.PIXELES_MM +64, body.getPosition().y*Constants.PIXELES_MM, 32, 32), world, Directions.RIGHT, 3f));
                lastShoot = shootVelocity;

        }

        if(lastShoot >= 0){
            lastShoot--;
        }
    }

    @Override
    public void onContact(Contact contact) {

    }

    @Override
    public void endContact(Contact contact) {

    }
}
