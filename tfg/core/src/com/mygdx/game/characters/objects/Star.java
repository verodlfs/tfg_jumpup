package com.mygdx.game.characters.objects;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.World;
import com.mygdx.game.MyGdxGame;
import com.mygdx.game.characters.DynamicElement;
import com.mygdx.game.managers.ResourceManager;
import com.mygdx.game.screens.ScoreScreen;
import com.mygdx.game.screens.WinScreen;
import com.mygdx.game.util.Constants;
import com.mygdx.game.util.ConstantsResources;

/**
 * Clase que nos llevará a la pantalla de ganador
 */
public class Star extends DynamicElement {

    final MyGdxGame game;

    private static Sound soundWin = ResourceManager.getSound(ConstantsResources.SOUND_WIN);

    private Texture texture;

    /**
     * Se inicializan las variables, se establecen las colisones,
     * se carga la textura correspondiente, y los objetos que van a poder
     * colisionar con el objeto de esta clase.
     * @param map    se pasa el mapa
     * @param rect	se pasa la colision
     * @param world	se pasa el mundo
     * @param game se pasa la pantalla game para poder
     *        acceder a sus variables y metodos
     */


    public Star(TiledMap map, Rectangle rect, World world, MyGdxGame game) {
        super(map, rect, world);
        this.game = game;
        fixtureDef.filter.categoryBits = Constants.STAR;
        //todas las cosas con las que va a colisionar
        fixtureDef.filter.maskBits = Constants.PLAYER;

        texture = ResourceManager.getTexture(ConstantsResources.STAR);
        createBodyRectangle( 10*Constants.SHIELD_BODY_SCALE/Constants.PIXELES_MM);
        body.getFixtureList().first().setRestitution(1f); // rebota
        body.setGravityScale(0);

    }

    /**
     * Se pinta la textura que se va a mostrar
     * @param dt tiempo entre cada frame que se renderiza
     * @param batch pinta la primera imagen de la textura activa
     */
    @Override
    public void draw(float dt, Batch batch) {

        batch.draw(
                texture,//textura
                body.getPosition().x + Constants.PLAYER_OFFSET_SPRITE_X  - (((texture.getWidth()*0.08f)/Constants.PIXELES_MM) * Constants.SHIELD_SCALE /2 ),//posicion x
                body.getPosition().y + Constants.PLAYER_OFFSET_SPRITE_Y - (((texture.getHeight()*0.08f)/Constants.PIXELES_MM) *Constants.SHIELD_SCALE /2), //posicion y
                ((texture.getWidth()*0.055f)/Constants.PIXELES_MM)*Constants.SHIELD_SCALE, //ancho
                ((texture.getHeight()*0.055f)/Constants.PIXELES_MM) *Constants.SHIELD_SCALE//alto

        );
    }

    @Override
    public void postDraw(float dt, Batch batch) {

    }

    @Override
    public void update(float dt) {

    }

    /**
     * El objeto con el que va a poder colisionar la estrella será el jugador,
     * cuando esto suceda se oirá un sonido y se abrirá una nueva ventana
     * en la que se mostrara que has ganado
     * @param contact player (objetos con los que va a colisionar)
     */
    @Override
    public void onContact(Contact contact) {
        if(contact.getFixtureA().getFilterData().categoryBits == Constants.PLAYER) {
            soundWin.play(0.3f);
            game.setScreen(new WinScreen(game));
        }

        if(contact.getFixtureB().getFilterData().categoryBits == Constants.PLAYER) {
            soundWin.play(0.3f);
            game.setScreen(new WinScreen(game));
        }

    }

    @Override
    public void endContact(Contact contact) {

    }

}
