package com.mygdx.game.characters.objects;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.World;
import com.mygdx.game.characters.DynamicElement;
import com.mygdx.game.managers.ResourceManager;
import com.mygdx.game.screens.GameScreen;
import com.mygdx.game.util.Constants;
import com.mygdx.game.util.ConstantsResources;
import com.mygdx.game.util.TextureUtil;

/**
 * Clase que da vidas al jugador
 */
public class Life extends DynamicElement {

    private static Sound soundLife = ResourceManager.getSound(ConstantsResources.SOUND_LIFE);

    private Animation<TextureRegion> lifeTexture;

    /**
     * Se inician las variables
     * Se indica con que objetos va a colisionar
     * Se establece la colision
     *
     * @param map  se carga el mapa
     * @param rect se carga la colision
     * @param world es la categoria del elemento
     */
    public Life(TiledMap map, Rectangle rect, World world) {
        super(map, rect, world);

        fixtureDef.filter.categoryBits = Constants.LIFE;
        fixtureDef.filter.maskBits = Constants.PLATFORM |Constants.PLAYER; // + Constants.enemy + ... ; todas las cosas con las que va a colisionar el personaje

        lifeTexture = new Animation<TextureRegion>(1/4f, ResourceManager.getAtlas(ConstantsResources.LIFE).findRegion("life"));

        createBodyRectangle( 10*Constants.LIFE_BODY_SCALE/Constants.PIXELES_MM);
        body.getFixtureList().first().setRestitution(0.7f); // revota
        body.setGravityScale(0);
    }

    /**
     * Se pinta la textura de la animacion que corresponda
     * @param delta tiempo entre cada frame que se renderiza
     * @param batch pinta la primera imagen de la textura activa
     */
    @Override
    public void draw(float delta, Batch batch) {
        TextureRegion tr = TextureUtil.getFrame(delta, lifeTexture);//obtenemos la imagen que corresonda de la animacion

        batch.draw(
                tr,//textura
                body.getPosition().x + Constants.PLAYER_OFFSET_SPRITE_X  - ((tr.getRegionHeight()/Constants.PIXELES_MM) * Constants.LIFE_SCALE/2 +0.1f),//posicion x
                body.getPosition().y + Constants.PLAYER_OFFSET_SPRITE_Y - ((tr.getRegionHeight()/Constants.PIXELES_MM) *Constants.LIFE_SCALE/2), //posicion y
                (tr.getRegionWidth()/Constants.PIXELES_MM)*Constants.LIFE_SCALE, //ancho
                (tr.getRegionHeight()/Constants.PIXELES_MM) *Constants.LIFE_SCALE//alto

        );
    }

    @Override
    public void postDraw(float dt, Batch batch) {

    }

    @Override
    public void update(float dt) {

    }

    /**
     * Cuando colisione con el jugador,
     * se destruirá la imagen y se oirá un sonido
     * @param contact  player (objetos con los que va a colisionar)
     */
    @Override
    public void onContact(Contact contact) {
        if(contact.getFixtureA().getFilterData().categoryBits == Constants.PLAYER) {
            soundLife.play(0.5f);
            this.toDestroy = true;
        }

        if(contact.getFixtureB().getFilterData().categoryBits == Constants.PLAYER) {
            soundLife.play(0.5f);
            this.toDestroy = true;
        }
    }

    @Override
    public void endContact(Contact contact) {

    }
}
