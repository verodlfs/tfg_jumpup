package com.mygdx.game.characters.objects;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.World;
import com.mygdx.game.characters.DynamicElement;
import com.mygdx.game.managers.ResourceManager;
import com.mygdx.game.util.Constants;
import com.mygdx.game.util.ConstantsResources;
import com.mygdx.game.util.Directions;
import com.mygdx.game.util.TextureUtil;

/**
 * Clase para movel plataformas y que no sean estaticas
 */
public class MovePlatform extends DynamicElement {

	private Animation<TextureRegion> animationWalk;

	public float enemyScale;
	public Vector2 movement;
	public Vector2 origin;
	public Vector2 movementSpeed;
	public Directions direction = Directions.LEFT;

	public float spriteSizeTargetValue=1f;
	public float spriteSizeTarget = 32;

	/**
	 * Se inician las variables, se establece un origen,
	 * se pinta la textura que se va a mostrar,
	 * los objetos con los que va a colisionar.
	 * @param map  	se pasa el mapa
	 * @param rect	se pasa la colision
	 * @param world	se pasa el mundo
	 * @param skinId se pasa el id de la imagen que se quiere mostrar
	 * @param enemyScale se da un tamaño a la imagen
	 */



	public MovePlatform(TiledMap map, Rectangle rect, World world, int skinId, float enemyScale) {
		super(map, rect, world);
		this.enemyScale = enemyScale;
		origin = new Vector2(rect.x/Constants.PIXELES_MM, rect.y/Constants.PIXELES_MM);
		movement = new Vector2(256/Constants.PIXELES_MM, 0);
		movementSpeed = new Vector2(0.02f, 0);

		fixtureDef.filter.categoryBits = Constants.MOVE_PLATFORM;
		fixtureDef.filter.maskBits = Constants.PLAYER;

		animationWalk = new Animation<TextureRegion>(1, ResourceManager.getAtlas(ConstantsResources.MOVE_PLATFORM[skinId]).findRegion("walk"));

		float bodySize = ((spriteSizeTarget*Constants.ENEMY_BODY_SCALE)/Constants.PIXELES_MM)*enemyScale;

		spriteSizeTargetValue = calculateSpriteSizeTarget(TextureUtil.getFrame(0,animationWalk).getRegionHeight());



		//La colision
		createBodyRectangle(bodySize*3.9f, bodySize*1.2f);

		body.setGravityScale(0);


	}

	/**
	 *  Calcula el tamaño de la imagen que se va a pintar
	 * @param height altura de la imagen
	 * @return spriteSizeTarget/height
	 */
	private float calculateSpriteSizeTarget(float height) {
		return spriteSizeTarget/height;
	}

	/**
	 * Se pinta la textura
	 * @param dt tiempo entre cada frame que se renderiza
	 * @param batch pinta la primera imagen de la textura activa
	 */
	@Override
	public void draw(float dt, Batch batch) {
		TextureRegion tr = TextureUtil.getFrame(dt, animationWalk);//obtenemos la imagen que corresonda de la animacion

		batch.draw(
				tr,//textura
				body.getPosition().x - (((spriteSizeTarget/2)*9)/Constants.PIXELES_MM)*enemyScale,//posicion x
				body.getPosition().y - (((spriteSizeTarget/2)*9)/Constants.PIXELES_MM)*enemyScale, //posicion y
				((spriteSizeTarget*9)/Constants.PIXELES_MM)*enemyScale, //ancho
				((spriteSizeTarget*9)/Constants.PIXELES_MM) *enemyScale );//alto

	}

	@Override
	public void postDraw(float dt, Batch batch) {


	}

	/**
	 * Se indica el movimiento que va a tener la plataforma,
	 * en caso de que colisione un objeto con ella se irá hacia abajo.
	 * @param dt tiempo entre cada frame que se renderiza
	 */
	@Override
	public void update(float dt) {
		if(direction == Directions.RIGHT && body.getPosition().x > origin.x + movement.x){
			body.setTransform(body.getPosition().x+movementSpeed.x,origin.y,0);

		}else {
			direction = Directions.DOWN;
		}

	}

	@Override
	public void onContact(Contact contact) {

	}



	@Override
	public void endContact(Contact contact) {


	}

}
