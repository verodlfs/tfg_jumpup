package com.mygdx.game.characters.player;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.World;
import com.mygdx.game.characters.DynamicElement;
import com.mygdx.game.managers.ResourceManager;
import com.mygdx.game.screens.GameScreen;
import com.mygdx.game.util.*;

public class Player extends MovementPlayer {

	public int lifes = 1;
	public int coin = 0;
	public int shield = 0;
	public int ultraShield = 0;
	private GameScreen gameScreen;

    // para cuando se sale del mapa y el jugador renace
	public float respownX = 0;
	public float respownY = 0;

	public float animationProgress;
	public Animations lastAnimation;
	public Animations actualAnimation;

	//shield options
	public float shieldDuration = 10f;
	public float shieldStart = 0f;

	public float ultraShieldDuration = 20f;
	public float ultraShieldStart = 0f;

	private static Sound soundDoor = ResourceManager.getSound(ConstantsResources.SOUND_DOOR);

    
    //ANIMACIONES
    private Animation<TextureRegion> animationWalk;
	private Animation<TextureRegion> animationWalkLeft;

	private Animation<TextureRegion> animationJump;
	private Animation<TextureRegion> animationJumpLeft;

	private Animation<TextureRegion> animationIdle;
	private Animation<TextureRegion> actualAnimationTexture;

	private Texture shieldTexture ;
	private Texture ultraShieldTexture;

	/**
	 * Se inicializan las variables, se indican las animaciones, objetos con los que va a colisionar el player,
	 * la forma de la colision...
	 * @param map se pasa el mapa
	 * @param rect se pasa la colision
	 * @param world se pasa el mundo
	 * @param gameScreen  se pasa la pantalla game para poder
	 * 					acceder a sus variables y metodos
	 */
	public Player(TiledMap map, Rectangle rect, World world, GameScreen gameScreen) {
		super(map, rect, world,gameScreen);
		this.gameScreen = gameScreen;
		fixtureDef.filter.categoryBits = Constants.PLAYER;
		fixtureDef.filter.maskBits = Constants.PLATFORM | Constants.ENEMY | Constants.COIN | Constants.LIFE
				| Constants.NEXT_LEVEL |  Constants.SHIELD | Constants.MOVE_PLATFORM | Constants.ULTRA_SHIELD | Constants.STAR ;

		//ANIMACIONES
		animationIdle = new Animation<TextureRegion>(0.05f, ResourceManager.getAtlas(ConstantsResources.PLAYER).findRegions("idle"));
		animationWalk = new Animation<TextureRegion>(0.05f, ResourceManager.getAtlas(ConstantsResources.PLAYER).findRegions("andar"));
		animationWalkLeft = new Animation<TextureRegion>(0.05f, ResourceManager.getAtlas(ConstantsResources.PLAYER).findRegions("left_andar"));
		animationJump = new Animation<TextureRegion>(0.1f, ResourceManager.getAtlas(ConstantsResources.PLAYER).findRegions("saltar"));
		animationJumpLeft = new Animation<TextureRegion>(0.1f, ResourceManager.getAtlas(ConstantsResources.PLAYER).findRegions("left_saltar"));

		shieldTexture = ResourceManager.getTexture(ConstantsResources.SHIELD_TEXTURE);
		ultraShieldTexture = ResourceManager.getTexture(ConstantsResources.ULTRASHIELD_TEXTURE);

		respownX = rect.x/Constants.PIXELES_MM;
		respownY = rect.y/Constants.PIXELES_MM;
		float bodysize =(TextureUtil.getFrame(0,animationWalk).getRegionWidth()/Constants.PIXELES_MM);
		createBody(bodysize*Constants.PLAYER_BODY_SCALE);

		CircleShape bodyShape2 = new CircleShape();
		bodyShape2.setRadius(bodysize*Constants.PLAYER_HEAD_SCALE);
		bodyShape2.setPosition(new Vector2(0f,bodysize/2));
		fixtureDef.filter.categoryBits = Constants.PLAYER_HEAD;
		fixtureDef.shape = bodyShape2;
		fixtureDef.filter.maskBits = Constants.PLATFORM;
		body.createFixture(fixtureDef).setUserData(this);

		System.out.println( ResourceManager.lifes);
		lifes =  ResourceManager.lifes;
		System.out.println( ResourceManager.coins);
		coin = ResourceManager.coins;
		
	}

	/**
	 * Se pintan las texturas de la animacion que correspondan;
	 * Cuando se muestra el ultra escudo y el escudo
	 * @param dt tiempo entre cada frame que se renderiza
	 * @param batch pinta la primera imagen de la textura activa
	 */
	@Override
	public void draw(float dt, Batch batch) {



		if(actualAnimation == lastAnimation){
			animationProgress = animationProgress + Gdx.graphics.getDeltaTime();
		}else{
			animationProgress = 0;
		}

		lastAnimation = actualAnimation;
		//Se obtiene la imagen que corresonda de la animacion
		TextureRegion tr = actualAnimationTexture.getKeyFrame(animationProgress, true);



		//se pinta la imagen del jugador que corresponda de la animacion
		batch.draw(
				tr,//textura 
				body.getPosition().x + Constants.PLAYER_OFFSET_SPRITE_X  - (((tr.getRegionHeight()*1.5f)/Constants.PIXELES_MM) * Constants.PLAYER_SCALE/2),//posicion x
				body.getPosition().y + Constants.PLAYER_OFFSET_SPRITE_Y - (((tr.getRegionHeight()*1.5f)/Constants.PIXELES_MM) *Constants.PLAYER_SCALE/2), //posicion y
				((tr.getRegionWidth()*1f)/Constants.PIXELES_MM)*Constants.PLAYER_SCALE, //ancho
				((tr.getRegionHeight()*1f)/Constants.PIXELES_MM) *Constants.PLAYER_SCALE//alto
		);

		if(ultraShield!=0){
			//mostrar el ultraescudo
			batch.draw(
					ultraShieldTexture,//textura
					body.getPosition().x + Constants.PLAYER_OFFSET_SPRITE_X  - ((tr.getRegionHeight()/Constants.PIXELES_MM) * Constants.SHIELD_SCALE_PLAYER/2)/*-64/Constants.PIXELES_MM*/,//posicion x
					body.getPosition().y + Constants.PLAYER_OFFSET_SPRITE_Y - (((tr.getRegionHeight()*6f)/Constants.PIXELES_MM) *Constants.SHIELD_SCALE_PLAYER/2), //posicion y
					((tr.getRegionWidth()*2)/Constants.PIXELES_MM)*Constants.SHIELD_SCALE_PLAYER , //ancho
					((tr.getRegionHeight()*4)/Constants.PIXELES_MM) *Constants.SHIELD_SCALE_PLAYER//alto

			);
		}
		if(shield!=0){
			//mostrar el escudo
			batch.draw(
					shieldTexture,//textura
					body.getPosition().x + Constants.PLAYER_OFFSET_SPRITE_X  - ((tr.getRegionHeight()/Constants.PIXELES_MM) * Constants.SHIELD_SCALE_PLAYER/2),//posicion x
					body.getPosition().y + Constants.PLAYER_OFFSET_SPRITE_Y - ((tr.getRegionHeight()/Constants.PIXELES_MM) *Constants.SHIELD_SCALE_PLAYER/2), //posicion y
					(tr.getRegionWidth()/Constants.PIXELES_MM)*Constants.SHIELD_SCALE_PLAYER , //ancho
					(tr.getRegionHeight()/Constants.PIXELES_MM) *Constants.SHIELD_SCALE_PLAYER//alto

			);
		}

	}

	@Override
	public void postDraw(float dt, Batch batch) {

		
		
	}

	/**
	 * Funcionamiento de los escudos, para que se controle su funcion
	 * Cuando el jugador se cae fuera del mapa lo redirecciona al punto
	 * de origen inicial
	 * @param dt tiempo entre cada frame que se renderiza
	 */
	@Override
	public void update(float dt) {
		manageInput();

		if(shield != 0){
			shieldStart++;
			if(shieldStart == 60){
				shield--;
				shieldStart = 0;
			}
		}

		if(ultraShield != 0){
			ultraShieldStart++;
			if(ultraShieldStart == 60){
				ultraShield--;
				ultraShieldStart = 0;
			}
		}

		//Cuando me caigo fuera del mapa
		if (body.getPosition().x<=0 ||body.getPosition().y<=0){
			lifes = 1;
			coin = 0;
			shield = 0;
			ultraShield = 0;
			body.setLinearVelocity(new Vector2(0,0));
			body.setTransform(new Vector2(respownX, respownY), 0);
		}

		if(lastShoot>=0){
			lastShoot--;
		}

		calculateActualAnimation();
	}

	/**
	 * Se indica lo que ocurre con cada cosa que colisiona con el jugador
	 * @param contact life, enemy, star, shield, ultrashield,
	 *                  platform, nextLevel (objetos con los que va a colisionar)
	 */
	@Override
	public void onContact(Contact contact) {


		if(contact.getFixtureA().getFilterData().categoryBits == Constants.LIFE) {
			this.lifes++;
		}

		if(contact.getFixtureB().getFilterData().categoryBits == Constants.LIFE) {
			this.lifes++;
		}

		if(contact.getFixtureA().getFilterData().categoryBits == Constants.ENEMY) {
			if(shield==0 && ultraShield == 0){
				this.lifes--;
			}else if(ultraShield !=0){
				((DynamicElement) contact.getFixtureA().getUserData()).toDestroy = true;
			}
		}

		if(contact.getFixtureB().getFilterData().categoryBits == Constants.ENEMY) {
			if(shield==0 && ultraShield == 0){
				this.lifes--;
			}else if(ultraShield !=0){
				((DynamicElement) contact.getFixtureB().getUserData()).toDestroy = true;
			}
		}

		if(contact.getFixtureA().getFilterData().categoryBits == Constants.STAR) {

				((DynamicElement) contact.getFixtureA().getUserData()).toDestroy = true;

		}

		if(contact.getFixtureB().getFilterData().categoryBits == Constants.STAR) {

				((DynamicElement) contact.getFixtureB().getUserData()).toDestroy = true;


		}

		if(contact.getFixtureA().getFilterData().categoryBits == Constants.SHIELD) {
			shield += (int) shieldDuration;
		}

		if(contact.getFixtureB().getFilterData().categoryBits == Constants.SHIELD) {
			shield += (int) shieldDuration;
		}

		if(contact.getFixtureA().getFilterData().categoryBits == Constants.ULTRA_SHIELD) {
			ultraShield += (int) ultraShieldDuration;
		}

		if(contact.getFixtureB().getFilterData().categoryBits == Constants.ULTRA_SHIELD) {
			ultraShield += (int) ultraShieldDuration;
		}


		if(contact.getFixtureA().getFilterData().categoryBits == Constants.PLATFORM && contact.getFixtureB().getFilterData().categoryBits == Constants.PLAYER) {

			isOnPlatform = true;
		}

		if(contact.getFixtureB().getFilterData().categoryBits == Constants.PLATFORM && contact.getFixtureA().getFilterData().categoryBits == Constants.PLAYER) {
			isOnPlatform = true;
		}

		if(contact.getFixtureA().getFilterData().categoryBits == Constants.MOVE_PLATFORM && contact.getFixtureB().getFilterData().categoryBits == Constants.PLAYER) {

			isOnPlatform = true;
			forceUp = forceUp*2;

		}

		if(contact.getFixtureB().getFilterData().categoryBits == Constants.MOVE_PLATFORM && contact.getFixtureA().getFilterData().categoryBits == Constants.PLAYER) {
			isOnPlatform = true;
			forceUp = forceUp*2;
		}

		if(contact.getFixtureA().getFilterData().categoryBits == Constants.NEXT_LEVEL) {
			soundDoor.play(0.3f);
			if(gameScreen!= null){
				gameScreen.nextLevel = true;
			}
		}

		if(contact.getFixtureB().getFilterData().categoryBits == Constants.NEXT_LEVEL) {
			soundDoor.play(0.3f);
			if(gameScreen!= null){
				gameScreen.nextLevel = true;
			}
		}

	}

	/**
	 * Cuando se finaliza el contacto se indica en este metodo,
	 * cambiando las variables necesarias
	 * @param contact platform, movePlatform (objetos con los que finaliza la colision)
	 */
	@Override
	public void endContact(Contact contact) {


		if(contact.getFixtureA().getFilterData().categoryBits == Constants.PLATFORM) {

			isOnPlatform = false;
		}

		if(contact.getFixtureB().getFilterData().categoryBits == Constants.PLATFORM) {
			isOnPlatform = false;
		}
		if(contact.getFixtureA().getFilterData().categoryBits == Constants.MOVE_PLATFORM) {

			isOnPlatform = false;
			forceUp = forceUp/2;
		}

		if(contact.getFixtureB().getFilterData().categoryBits == Constants.MOVE_PLATFORM) {
			isOnPlatform = false;
			forceUp = forceUp/2;
		}


	}

	/**
	 * Se calcula la animacion que se necesita en cada momento
	 */
	private void calculateActualAnimation(){

		if(body.getLinearVelocity().y == 0) {
			if (body.getLinearVelocity().x == 0) {
				actualAnimation = Animations.IDLE;
				actualAnimationTexture = animationIdle;
			}else if(body.getLinearVelocity().x > 0){
				actualAnimation = Animations.WALK_RIGHT;
				actualAnimationTexture = animationWalk;
			}else if(body.getLinearVelocity().x < 0){
				actualAnimation = Animations.WALK_LEFT;
				actualAnimationTexture = animationWalkLeft;
			}
		}else{
			if(body.getLinearVelocity().x > 0){
				actualAnimation = Animations.JUMP_RIGHT;
				actualAnimationTexture = animationJump;
			}else if(body.getLinearVelocity().x < 0){
				actualAnimation = Animations.JUMP_LEFT;
				actualAnimationTexture = animationJumpLeft;
			}
		}
	}
	
}
