package com.mygdx.game.characters.player;

import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.mygdx.game.characters.DynamicElement;
import com.mygdx.game.characters.bullets.Shoot;
import com.mygdx.game.screens.GameScreen;
import com.mygdx.game.util.Constants;
import com.mygdx.game.util.Directions;

/**
 * Clase que da movimiento al jugador
 */
public abstract class MovementPlayer extends DynamicElement {

    //movimiento direcciones
    public boolean up = false;
    public boolean down = false;
    public boolean left = false;
    public boolean right = false;

	public boolean bullet_up = false;
	public boolean bullet_down = false;
	public boolean bullet_left = false;
	public boolean bullet_right = false;

    //estados del movimiento
    public boolean isOnPlatform = true;

    //proiedades del movimiento
    public float forceUp = 3.1f;

    //disparos
    public boolean shoot = false;

    public float lastShoot = 0;
    public float shootVelocity = 5f;


    public GameScreen gameScreen;

	/**
	 * Constructor donde se inicializan las variables
	 * @param map se pasa el mapa
	 * @param rect se pasa la colision
	 * @param world se pasa el mundo
	 * @param gameScreen se pasa la pantalla game para poder
	 * 					acceder a sus variables y metodos
	 */
    public MovementPlayer(TiledMap map, Rectangle rect, World world,GameScreen gameScreen) {
        super(map, rect, world);
		this.gameScreen = gameScreen;
    }


	/**
	 * Se maneja el salto del jugador
	 */
    protected void manageInput() {
        if (up && isOnPlatform) {
            body.applyLinearImpulse(new Vector2(0f, forceUp), body.getWorldCenter(), true);
        }
        if (left && body.getLinearVelocity().x >= -1.1f) {
            body.applyLinearImpulse(new Vector2(0.31f, 0f), body.getWorldCenter(), true);
        }
        if (right && body.getLinearVelocity().x <= 1.1f) {
            body.applyLinearImpulse(new Vector2(-0.31f, 0f), body.getWorldCenter(), true);
        }


		shoot();


	}

	/**
	 * Se maneja el disparo, dependiendo de la direccion hacia la que vaya la bala
	 */
	private void shoot() {
		if (lastShoot <= 0 && shoot) {
			if (bullet_right) {
				gameScreen.levelManager.enemies.add(
						new Shoot(map, new Rectangle(body.getPosition().x * Constants.PIXELES_MM, body.getPosition().y *Constants.PIXELES_MM, 32, 32), world, Directions.RIGHT,8f));
				lastShoot = shootVelocity;


			} else if (bullet_left) {
				gameScreen.levelManager.enemies.add(
						new Shoot(map, new Rectangle(body.getPosition().x *Constants.PIXELES_MM, body.getPosition().y *Constants.PIXELES_MM, 32, 32), world, Directions.LEFT,8f));
				lastShoot = shootVelocity;


			} else if(bullet_up) {
				gameScreen.levelManager.enemies.add(
						new Shoot(map, new Rectangle(body.getPosition().x *Constants.PIXELES_MM, body.getPosition().y *Constants.PIXELES_MM, 32, 32), world, Directions.UP,8f));
				lastShoot = shootVelocity;
			} else if(bullet_down) {
				gameScreen.levelManager.enemies.add(
						new Shoot(map, new Rectangle(body.getPosition().x *Constants.PIXELES_MM, body.getPosition().y *Constants.PIXELES_MM, 32, 32), world, Directions.DOWN,8f));
				lastShoot = shootVelocity;
			}
		}
	}

}
