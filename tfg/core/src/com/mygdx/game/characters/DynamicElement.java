package com.mygdx.game.characters;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g3d.shaders.DefaultShader.Setters.Bones;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import com.mygdx.game.util.Constants;
import com.mygdx.game.util.Directions;

/**
 * Clase abstracta para los elementos dinamicos
 */
public abstract class DynamicElement extends Contact{
	
	protected TiledMap map;
	protected Rectangle rect;
	public Body body;
	protected World world;
	protected BodyDef bodyDef;
	protected FixtureDef fixtureDef;
	
	public boolean toDestroy;

	/**
	 * Se inicializa el constructor
	 * @param map se pasa el mapa
	 * @param rect se pasa la colision
	 * @param world se pasa el mundo
	 */
	public DynamicElement (TiledMap map, Rectangle rect, World world) {
		this.map = map;
		this.rect = rect;
		this.world = world;
		this.toDestroy = false;
		this.fixtureDef = new FixtureDef();
		this.bodyDef = new BodyDef();
		

	}


	/**
	 * Este metodo es para establecer las colisiones
	 * de cada objeto en su propia clase el
	 * parametro que se pasará será un float
	 * @param bodySize tamano del objeto
	 */
	public void createBody(float bodySize) {
		bodyDef.type = BodyDef.BodyType.DynamicBody;
		bodyDef.position.set(new Vector2(rect.getX()/Constants.PIXELES_MM, rect.getY()/Constants.PIXELES_MM ));
		body = world.createBody(bodyDef);
		
		CircleShape bodyShape = new CircleShape();
		bodyShape.setRadius(bodySize/2 );

		fixtureDef.shape = bodyShape;
		body.createFixture(fixtureDef).setUserData(this);

	}

	/**
	 * Creacion de una colision rectangular
	 * @param bodySize tamano del objeto
	 */
	public void createBodyRectangle(float bodySize) {
		createBodyRectangle(bodySize, bodySize);
	}

	/**
	 * Se crea una colision rectangular
	 * @param bodySizex tamano x del objeto
	 * @param bodySizey tamano y del objeto
	 */
	public void createBodyRectangle(float bodySizex, float bodySizey) {
		bodyDef.type = BodyDef.BodyType.DynamicBody;
		bodyDef.position.set(new Vector2(rect.getX()/Constants.PIXELES_MM, rect.getY()/Constants.PIXELES_MM ));
		body = world.createBody(bodyDef);

		PolygonShape bodyShape = new PolygonShape();
		bodyShape.setAsBox(bodySizex ,bodySizey);

		fixtureDef.shape = bodyShape;
		body.createFixture(fixtureDef).setUserData(this);

	}


	public void destroyBody() {
		world.destroyBody(body);
		
	}

	/**
	 * Devuelve la direcion actual del cuerpo
	 * @return Directions direccion en la que se mueve el cuerpo
	 */

	public Directions getDirection(){
		if(body.getLinearVelocity().y == 0 && body.getLinearVelocity().x == 0) {
			return Directions.IDLE;
		}else if(body.getLinearVelocity().y < 0 && body.getLinearVelocity().x == 0) {
			return Directions.DOWN;
		}else if(body.getLinearVelocity().x < 0) {
			return Directions.LEFT;
		}else if(body.getLinearVelocity().x > 0) {
			return Directions.RIGHT;
		}
		return Directions.IDLE;
	}

	/**
	 * Metodo abstracto que permitirá pintar la textura
	 * @param dt tiempo entre cada frame que se renderiza
	 * @param batch pinta la primera imagen de la textura activa
	 */
	public abstract void draw(float dt, Batch batch);
    public abstract void postDraw(float dt, Batch batch);

	/**
	 * Metodo que actualizará el objeto
	 * @param dt tiempo entre cada frame que se renderiza
	 */
	public abstract void update(float dt);
	

}
