package com.mygdx.game.characters;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.mygdx.game.screens.GameScreen;
import com.mygdx.game.util.Constants;
/**
 * Clase abstracta para los elementos estaticos
 */
public abstract class StaticElement extends Contact{

	//Definicion del cuerpo que va a tener
	protected BodyDef bodyDef = new BodyDef();
	
	public PolygonShape shape = new PolygonShape();
	
	protected FixtureDef fixtureDef = new FixtureDef();
	
	
	public Body body;
	
	//Que desaparezca un objeto del mapa
	public boolean toDestroy;
	
	/**
	 * Se incializan las variables, se anade un cuerpo nuevo
	 * y se establece el agarre a la plataforma
	 * @param rect se pasa la colision
	 * @param game se pasa la pantalla game para poder
	 *             acceder a sus variables y metodos
	 * @param category es la categoria del elemento
	 */
	public StaticElement(Rectangle rect, GameScreen game, short category) {
		toDestroy = true;
		bodyDef.type = BodyDef.BodyType.StaticBody;
		bodyDef.position.set((rect.getX() + rect.getWidth()/2)/Constants.PIXELES_MM,(rect.getY() + rect.getHeight()/2)/Constants.PIXELES_MM);
		
		//Anadir un nuevo cuerpo
		body = game.world.createBody(bodyDef);
		shape.setAsBox(rect.getWidth()/2/Constants.PIXELES_MM, rect.getHeight()/2/Constants.PIXELES_MM);
		fixtureDef.shape = shape;
		//agarre a la plataforma
		fixtureDef.friction = 5;
		fixtureDef.filter.categoryBits = category;
		body.createFixture(fixtureDef).setUserData(this);
		
	}

	/**
	 * Se pinta la textura de los elementos estaticos
	 * @param delta tiempo entre cada frame que se renderiza
	 * @param batch pinta la primera imagen de la textura activa
	 */
	public abstract void draw(float delta, Batch batch);

}
