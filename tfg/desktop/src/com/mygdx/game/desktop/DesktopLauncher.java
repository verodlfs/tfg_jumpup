package com.mygdx.game.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.mygdx.game.MyGdxGame;

/**
 * Clase desde la que se lanza el juego
 */
public class DesktopLauncher {
	public static void main (String[] arg) {
		new LwjglApplication(new MyGdxGame(), "JUMP UP!", 800, 1000);
	}
}
